#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define SAMPLE_SIZE 100000
#define FILTER_SIZE 9
#define BLOCKS 1000
#define THREADS_PER_BLOCK 256

void initData(int * arrA, int * coeff, int sampleSize) {
    for (int idx=0;idx<sampleSize;idx++)
        arrA[idx] = 1 + ( rand() % (100 - 1 + 1));
    for (int idx=0;idx<FILTER_SIZE;idx++)
        coeff[idx] = 1;//1 + ( rand() % (100 - 1 + 1));
}

void FIR_filter_direct_cpu(int * arrA, int * arrB, int * coeff, int sampleSize) {

    for (int idx=0;idx<sampleSize;idx++){
        int acc = 0;
        for (int s=0;s<FILTER_SIZE;s++) {
            acc += ((idx - s) < 0) ? 0 : coeff[s] * arrA[idx-s];
        }
        arrB[idx] = acc;
    }
}

bool verifyResult(int * arrB, int * arrBv, int sampleSize, int * failedIndex) {

    for (int idx=0;idx<sampleSize;idx++) {
        if (arrB[idx] != arrBv[idx]) {
            *failedIndex = idx;
            return false;
        }
    }

    return true;
}

__global__ void FIR_filter_direct(int * arrA, int * arrB, int * coeff, int sampleSize) {

    int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
    int acc = 0;
    if (idx >= sampleSize)
        return;
    for (int s=0;s<FILTER_SIZE;s++) {
        acc += ((idx - s) < 0) ? 0 : coeff[s] * arrA[idx-s];
    }
    arrB[idx] = acc;
}

int main(int argc, char ** argv) {

    int * arrA_h, * arrB_h, * arrBv, * coeff_h;
    int * arrA_d, * arrB_d, * coeff_d;

    arrA_h  = new int [SAMPLE_SIZE];
    arrB_h  = new int [SAMPLE_SIZE];
    arrBv   = new int [SAMPLE_SIZE];
    coeff_h = new int [FILTER_SIZE];
    initData(arrA_h, coeff_h, SAMPLE_SIZE);
    FIR_filter_direct_cpu(arrA_h, arrBv, coeff_h, SAMPLE_SIZE);
#if 0
    for (int idx=0;idx<SAMPLE_SIZE;idx++) {
        cout << arrA_h[idx] << " / " << arrBv[idx] << endl;
    }
#endif

    cudaMalloc((void **) &arrA_d, SAMPLE_SIZE * sizeof(int));
    cudaMalloc((void **) &arrB_d, SAMPLE_SIZE * sizeof(int));
    cudaMalloc((void **) &coeff_d, FILTER_SIZE * sizeof(int));

    cudaMemcpy(arrA_d, arrA_h, SAMPLE_SIZE * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(coeff_d, coeff_h, FILTER_SIZE * sizeof(int), cudaMemcpyHostToDevice);

    FIR_filter_direct <<<BLOCKS, THREADS_PER_BLOCK>>> (arrA_d, arrB_d, coeff_d, SAMPLE_SIZE);
    cudaDeviceSynchronize();

    cudaMemcpy(arrB_h, arrB_d, SAMPLE_SIZE * sizeof(int), cudaMemcpyDeviceToHost);

    int failedIndex = -1;
    cout << "Verification: ";
    if (verifyResult(arrB_h, arrBv, SAMPLE_SIZE, &failedIndex))
        cout << "PASSED";
    else
        cout << "FAILED at index [" << failedIndex << "]";
    cout << endl;

    return 0;
}
