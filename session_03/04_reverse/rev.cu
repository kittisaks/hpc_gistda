#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define SAMPLE_SIZE 100000
#define FILTER_SIZE 9
#define BLOCKS 1000
#define THREADS_PER_BLOCK 256

void initData(int * arrA, int sampleSize) {
    for (int idx=0;idx<sampleSize;idx++)
        arrA[idx] = 1 + ( rand() % (100 - 1 + 1));
}


bool verifyResult(int * arrA, int * arrB, int sampleSize, int * failedIndex) {

#if 1
    for (int idx=0;idx<sampleSize;idx++) {
        if (arrA[idx] != arrB[sampleSize - idx - 1]) {
            *failedIndex = idx;
            return false;
        }
    }
#endif

    return true;
}

__global__ void reverse(int * arrA, int * arrB, int sampleSize) {

    //TODO: Add kernel code here
}

int main(int argc, char ** argv) {

    int * arrA_h, * arrB_h;
    int * arrA_d, * arrB_d;

    arrA_h  = new int [SAMPLE_SIZE];
    arrB_h  = new int [SAMPLE_SIZE];
    initData(arrA_h, SAMPLE_SIZE);

    //TODO: Allocate device memory

    //TODO: Memory transfer

    //TODO: Kernel call

    //TODO: Memory transfer

    int failedIndex = -1;
    cout << "Verification: ";
    if (verifyResult(arrA_h, arrB_h, SAMPLE_SIZE, &failedIndex))
        cout << "PASSED";
    else
        cout << "FAILED at index [" << failedIndex << "]";
    cout << endl;

    return 0;
}
