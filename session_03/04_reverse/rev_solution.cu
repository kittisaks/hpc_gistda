#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define SAMPLE_SIZE 100000
#define FILTER_SIZE 9
#define BLOCKS 1000
#define THREADS_PER_BLOCK 256

void initData(int * arrA, int sampleSize) {
    for (int idx=0;idx<sampleSize;idx++)
        arrA[idx] = 1 + ( rand() % (100 - 1 + 1));
}


bool verifyResult(int * arrA, int * arrB, int sampleSize, int * failedIndex) {

#if 1
    for (int idx=0;idx<sampleSize;idx++) {
        if (arrA[idx] != arrB[sampleSize - idx - 1]) {
            *failedIndex = idx;
            return false;
        }
    }
#endif

    return true;
}

__global__ void reverse(int * arrA, int * arrB, int sampleSize) {

    int idx = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (idx >= sampleSize)
        return;
    arrB[sampleSize - idx - 1] = arrA[idx];
}

int main(int argc, char ** argv) {

    int * arrA_h, * arrB_h;
    int * arrA_d, * arrB_d;

    arrA_h  = new int [SAMPLE_SIZE];
    arrB_h  = new int [SAMPLE_SIZE];
    initData(arrA_h, SAMPLE_SIZE);

    cudaMalloc((void **) &arrA_d, SAMPLE_SIZE * sizeof(int));
    cudaMalloc((void **) &arrB_d, SAMPLE_SIZE * sizeof(int));

    cudaMemcpy(arrA_d, arrA_h, SAMPLE_SIZE * sizeof(int), cudaMemcpyHostToDevice);

    reverse <<<BLOCKS, THREADS_PER_BLOCK>>> (arrA_d, arrB_d, SAMPLE_SIZE);
    cudaDeviceSynchronize();

    cudaMemcpy(arrB_h, arrB_d, SAMPLE_SIZE * sizeof(int), cudaMemcpyDeviceToHost);

    int failedIndex = -1;
    cout << "Verification: ";
    if (verifyResult(arrA_h, arrB_h, SAMPLE_SIZE, &failedIndex))
        cout << "PASSED";
    else
        cout << "FAILED at index [" << failedIndex << "]";
    cout << endl;

    return 0;
}
