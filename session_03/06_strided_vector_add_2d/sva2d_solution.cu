#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define dtype int
#define uint unsigned int

#define VECTOR_DIM 100000
#define BLOCKS_X 5
#define BLOCKS_Y 5
#define THREADS_PER_BLOCK_X 8
#define THREADS_PER_BLOCK_Y 8

void initializeVectors(dtype * vecA, dtype * vecB, uint dim) {

#define MIN_NUM 1
#define MAX_NUM 100

    for (uint idx=0;idx<dim;idx++) {
        vecA[idx] = static_cast<dtype>(1 + ( rand() % ( MAX_NUM - MIN_NUM + 1) ));
        vecB[idx] = static_cast<dtype>(1 + ( rand() % ( MAX_NUM - MIN_NUM + 1) ));
    }

#undef MIN_NUM
#undef MAX_NUM
}

void vectorAddCPU(dtype * vecA, dtype * vecB, dtype * vecC, uint dim) {

    for (uint idx=0;idx<dim;idx++) {
        vecC[idx] = vecA[idx] + vecB[idx];
    }
}

bool verifyResult(dtype * vecC_compare, dtype * vecC_standard, uint dim) {

    for (uint idx=0;idx<dim;idx++) {
        if (vecC_compare[idx] != vecC_standard[idx]) {
            cerr << "[Error]: Results at index " << idx << " do not match!" << endl;
            return false;
        }
    }
    return true;
}

__global__ void vectorAdd(dtype * vecA, dtype * vecB, dtype * vecC, uint dim) {


    //Flatten the blocks and threads into one-dimensional space
    uint tid = (((blockIdx.y * gridDim.x) + blockIdx.x) * blockDim.x * blockDim.y) + ((threadIdx.y * blockDim.x) + threadIdx.x);
    uint totalThreads = gridDim.y * gridDim.x * blockDim.x * blockDim.y;
    uint totalStride = dim / totalThreads;
    totalStride += (dim % totalThreads) ? 1 : 0; 

    for (uint sIdx=0;sIdx<totalStride;sIdx++) {
        uint vIdx = (sIdx * totalThreads) + tid;
        vecC[vIdx] = vecA[vIdx] + vecB[vIdx];
    }

}

int main(int argc, char ** argv) {

    dtype * vecA_h, * vecB_h, * vecC_h, * vecCv;
    dtype * vecA_d, * vecB_d, * vecC_d;

    size_t memSize = sizeof(dtype) * VECTOR_DIM;

    //Allocate host memory
    vecA_h = new dtype [VECTOR_DIM];
    vecB_h = new dtype [VECTOR_DIM];
    vecC_h = new dtype [VECTOR_DIM];
    vecCv  = new dtype [VECTOR_DIM];
    initializeVectors(vecA_h, vecB_h, VECTOR_DIM);
    memset(vecC_h, 0, memSize);

    //Allocate device memory
    cudaMalloc(&vecA_d, memSize);
    cudaMalloc(&vecB_d, memSize);
    cudaMalloc(&vecC_d, memSize);
    cudaMemcpy(vecA_d, vecA_h, memSize, cudaMemcpyHostToDevice);
    cudaMemcpy(vecB_d, vecB_h, memSize, cudaMemcpyHostToDevice);
    cudaMemset(vecC_d, 0, memSize);

    dim3 blocks(BLOCKS_X, BLOCKS_Y, 1);
    dim3 threads(THREADS_PER_BLOCK_X, THREADS_PER_BLOCK_Y, 1);
    vectorAdd <<<blocks, threads>>> (vecA_d, vecB_d, vecC_d, VECTOR_DIM);
    cudaDeviceSynchronize();

    cudaMemcpy(vecC_h, vecC_d, memSize, cudaMemcpyDeviceToHost);

    //Verifying the result from GPU with the result from CPU
    vectorAddCPU(vecA_h, vecB_h, vecCv, VECTOR_DIM);
    cout << "Verification: ";
    if (!verifyResult(vecC_h, vecCv, VECTOR_DIM))
        cout << "FAILED";
    else
        cout << "PASSED";
    cout << endl;
    return 0;
}

