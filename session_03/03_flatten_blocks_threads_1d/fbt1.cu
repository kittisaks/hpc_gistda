#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define BLOCK_CNT 20
#define THREAD_PER_BLOCK_CNT 256

__global__ void flatten_blocks_threads_1d(int * result, int * blockResult, int * threadResult) {

    //TODO: Add kernel code here
}

int main(int argc, char ** argv) {

    int * result_h, * result_d;
    int * blockResult_h, * blockResult_d;
    int * threadResult_h, * threadResult_d;

    int resultSize = BLOCK_CNT * THREAD_PER_BLOCK_CNT;
    int memSize = resultSize * sizeof(int);

    //Allocate host memory
    result_h = new int [resultSize];
    blockResult_h = new int [resultSize];
    threadResult_h = new int [resultSize];

    //TODO: Allocate and initialize device memory

    //TODO: Invoke device kernel

    //TODO: Copy the results from device back to host

    //Display the results
    for (int i=0;i<resultSize;i++)
        cout << result_h[i] << "\t" << blockResult_h[i] << "\t" << threadResult_h[i] << endl;

    cudaFree(result_d);
    delete [] result_h;

    return 0;
}

