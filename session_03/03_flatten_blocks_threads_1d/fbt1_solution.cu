#include <cuda_runtime_api.h>

#include <iostream>

using namespace std;

#define BLOCK_CNT 20
#define THREAD_PER_BLOCK_CNT 256

__global__ void flatten_blocks_threads_1d(int * result, int * blockResult, int * threadResult) {

    int resultIdx = (blockIdx.x * blockDim.x) + threadIdx.x;
    result[resultIdx] = resultIdx;
    blockResult[resultIdx] = blockIdx.x;
    threadResult[resultIdx] = threadIdx.x;
}

int main(int argc, char ** argv) {

    int * result_h, * result_d;
    int * blockResult_h, * blockResult_d;
    int * threadResult_h, * threadResult_d;

    int resultSize = BLOCK_CNT * THREAD_PER_BLOCK_CNT;
    int memSize = resultSize * sizeof(int);

    //Allocate host memory
    result_h = new int [resultSize];
    blockResult_h = new int [resultSize];
    threadResult_h = new int [resultSize];

    //Allocate and initialize device memory
    cudaMalloc(&result_d, memSize);
    cudaMalloc(&blockResult_d, memSize);
    cudaMalloc(&threadResult_d, memSize);
    cudaMemset(result_d, 0, memSize);
    cudaMemset(blockResult_d, 0, memSize);
    cudaMemset(threadResult_d, 0, memSize);

    //Invoke device kernel
    flatten_blocks_threads_1d <<<BLOCK_CNT, THREAD_PER_BLOCK_CNT>>> (result_d, blockResult_d, threadResult_d);
    cudaDeviceSynchronize();

    //Copy the results from device back to host
    cudaMemcpy(result_h, result_d, memSize, cudaMemcpyDeviceToHost);
    cudaMemcpy(blockResult_h, blockResult_d, memSize, cudaMemcpyDeviceToHost);
    cudaMemcpy(threadResult_h, threadResult_d, memSize, cudaMemcpyDeviceToHost);

    //Display the results
    for (int i=0;i<resultSize;i++)
        cout << result_h[i] << "\t" << blockResult_h[i] << "\t" << threadResult_h[i] << endl;

    cudaFree(result_d);
    delete [] result_h;

    return 0;
}

