
CXX = g++
MPICXX = mpicxx
NVCC = nvcc
LD = g++
RM = rm
CP = cp
AR = ar

MPI_INC_PATH = /usr/local/openmpi/include
CUDA_INC_PATH = /usr/local/cuda/include
MPI_BIN_PATH = /usr/local/openmpi/bin
CUDA_BIN_PATH = /usr/local/cuda/bin
MPI_LIB_PATH = /usr/local/openmpi/lib
CUDA_LIB_PATH = /usr/local/cuda/lib64

MPI_INC_FLAGS = $(addprefix -I , $(MPI_INC_PATH))

CXX_PRE_FLAGS = $(MPI_INC_FLAGS) -Wall -O2
CXX_POST_FLAGS =
NVCC_PRE_FLAGS =
NVCC_POST_FLAGS =
LD_PRE_FLAGS = $(addprefix -L , $(MPI_LIB_PATH))
LD_POST_FLAGS = -lmpi
RM_FLAGS = -f
CP_FLAGS =
AR_FLAGS = -rcs

.PHONY: all clean

