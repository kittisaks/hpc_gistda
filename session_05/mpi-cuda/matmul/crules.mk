obj/%.o:%.cpp
	$(CXX) -c -MD $(CXX_PRE_FLAGS) $< -o $@ $(CXX_POST_FLAGS)

obj/%.o:%.cu
	$(NVCC) -c $(NVCC_PRE_FLAGS) -Xcompiler="$(CXX_PRE_FLAGS)" $< -o $@ $(NVCC_POST_FLAGS)
