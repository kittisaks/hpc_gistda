#include "matbin.h"
#include <iostream>

using namespace std;

int matmulHost(
    dtype ** matA, uint matAy, uint matAx,
    dtype ** matB, uint matBy, uint matBx, 
    dtype ** matC, uint matCy, uint matCx) {

    for (uint i=0;i<matAy;i++) {
        for (uint j=0;j<matBx;j++) {
            dtype acc = 0.0;
            for (uint k=0;k<matAx;k++) {
                //cout << i << "," << k << " : " << k << "," << j << endl;
                acc += matA[i][k] * matB[k][j];
            }
            matC[i][j] = acc;
        }
    }

    return 0;
}


