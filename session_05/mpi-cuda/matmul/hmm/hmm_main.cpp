#include <stdlib.h>

#include <iostream>

#include "matbin.h"
#include "hmm.h"

using namespace std;

#define MAT_A_FILE "matA_Y3000_X3000.bin" 
#define MAT_B_FILE "matB_Y3000_X3000.bin"
#define MAT_C_FILE "matC_Y3000_X3000.bin"

inline void checkErrorLoadMatrix(int ret, const char * filename) {
    if (!ret)
        return;
    cout << "[Error]: Cannot load " << filename << endl;
    exit(-1);
}

bool verifyMatEqual(dtype * mat, dtype * ref, uint dy, uint dx, uint * failedIdx, double * failedDiff) {

    uint totalLength = dy * dx;
    for (uint idx=0;idx<totalLength;idx++) {
        dtype diff = abs(mat[idx] - ref[idx]);
        double pdiff = ((double) diff) / ((double) ref[idx]);
        if (pdiff > 0.08) {
            cout << mat[idx] << " / " << ref[idx] << endl;
            *failedDiff = pdiff;
            *failedIdx = idx;
            return false;
        }
    }
    return true;
}

int main(int argc, char ** argv) {
    
    dtype ** matA_h;
    uint  matAx, matAy;
    dtype ** matB_h;
    uint  matBx, matBy;
    dtype ** matC_h, ** matCv;
    uint  matCx, matCy;

    checkErrorLoadMatrix(readMatBinaryFile(MAT_A_FILE, &matA_h, &matAy, &matAx), MAT_A_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_B_FILE, &matB_h, &matBy, &matBx), MAT_B_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_C_FILE, &matCv, &matCy, &matCx), MAT_C_FILE);
    createMat(&matC_h, matCy, matCx);

    cout << "Matrix A (YxX):  " << matAy << "x" << matAx << endl;
    cout << "Matrix B (YxX):  " << matBy << "x" << matBx << endl;
    cout << "Matrix C (YxX):  " << matCy << "x" << matCx << endl;

    matmulHost(matA_h, matAy, matAx, matB_h, matBy, matBx, matC_h, matCy, matCx);

    uint failedIdx;
    double failedDiff;
    bool isPassed = verifyMatEqual(matC_h[0], matCv[0], matCy, matCx, &failedIdx, &failedDiff);
    cout << "Verification: ";
    if (isPassed)
        cout << "PASSED" << endl;
    else
        cout << "FAILED at index [" << failedIdx << "], diff [" << failedDiff << "]" << endl;

    return 0;
}

