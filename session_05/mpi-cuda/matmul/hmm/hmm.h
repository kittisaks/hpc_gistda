#ifndef _HMM_H
#define _HMM_H

int matmulHost(
    dtype ** matA, uint matAy, uint matAx,
    dtype ** matB, uint matBy, uint matBx, 
    dtype ** matC, uint matCy, uint matCx);

#endif /* _HMM_H */
