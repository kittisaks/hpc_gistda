#include <stdlib.h>
#include "timer.h"

void startTimer(timer * tm) {
    gettimeofday(&tm->startTime, NULL);
    tm->isRunning = true;
}

void stopTimer(timer * tm) {
    gettimeofday(&tm->endTime, NULL);
    tm->isRunning = false;
}

double getTimerDuration(timer * tm) {

    if (tm->isRunning)
        return 0.0;

    double duration;

    duration = (tm->endTime.tv_sec - tm->startTime.tv_sec) * 1e6;
    duration = (duration + (tm->endTime.tv_usec -
                              tm->startTime.tv_usec)) * 1e-6;
    return duration;
}

bool isTimerRunning(timer * tm) {
    return tm->isRunning;
}




