#ifndef _COMMON_H
#define _COMMON_H

#include "matbin.h"
#include "timer.h"
#include "hmm/hmm.h"
#include "dmm/dmm.h"

typedef struct {
    uint matAy;
    uint matAx;
    uint matBy;
    uint matBx;
    uint matCy;
    uint matCx;
} MatInfo_t;

typedef struct {
    uint        startRow;
    uint        rowCount;
    timer       tm;
} RankInfo_t;

#endif /* _COMMON_H */

