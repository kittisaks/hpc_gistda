#ifndef _DMM_H
#define _DMM_H

int matmulDevice(
    dtype ** matA_h, uint matAy, uint matAx,
    dtype ** matB_h, uint matBy, uint matBx, 
    dtype ** matC_h, uint matCy, uint matCx);

#endif /* _DMM_H */
