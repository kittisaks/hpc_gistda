#include <mpi.h>
#include <stdlib.h>

#include <iostream>

#include "common.h"
#include "timer.h"

using namespace std;

#define MAT_A_FILE "matA_Y3000_X3000.bin" 
#define MAT_B_FILE "matB_Y3000_X3000.bin"
#define MAT_C_FILE "matC_Y3000_X3000.bin"

inline void checkErrorLoadMatrix(int ret, const char * filename) {
    if (!ret)
        return;
    cout << "[Error]: Cannot load " << filename << endl;
    exit(-1);
}

bool verifyMatEqual(dtype * mat, dtype * ref, uint dy, uint dx, uint * failedIdx, double * failedDiff) {

    uint totalLength = dy * dx;
    for (uint idx=0;idx<totalLength;idx++) {
        dtype diff = abs(mat[idx] - ref[idx]);
        double pdiff = ((double) diff) / ((double) ref[idx]);
        if (pdiff > 0.09) {
            cout << mat[idx] << " / " << ref[idx] << endl;
            *failedDiff = pdiff;
            *failedIdx = idx;
            return false;
        }
    }
    return true;
}

void loadDistributor(int rank, int commSize) {
    cout << "[LDIST-" << rank << "]: Started" << endl;

    dtype ** matA;
    uint  matAx, matAy;
    dtype ** matB;
    uint  matBx, matBy;
    dtype ** matC, ** matCv;
    uint  matCx, matCy;

    checkErrorLoadMatrix(readMatBinaryFile(MAT_A_FILE, &matA, &matAy, &matAx), MAT_A_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_B_FILE, &matB, &matBy, &matBx), MAT_B_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_C_FILE, &matCv, &matCy, &matCx), MAT_C_FILE);
    createMat(&matC, matCy, matCx);

    cout << "Matrix A (YxX):  " << matAy << "x" << matAx << endl;
    cout << "Matrix B (YxX):  " << matBy << "x" << matBx << endl;
    cout << "Matrix C (YxX):  " << matCy << "x" << matCx << endl;

   
    //Broadcast matrix information
    MatInfo_t matInfo;
    matInfo.matAy = matAy;
    matInfo.matAx = matAx;
    matInfo.matBy = matBy;
    matInfo.matBx = matBx;
    matInfo.matCy = matCy;
    matInfo.matCx = matCx;
    MPI_Bcast(&matInfo, sizeof(MatInfo_t), MPI_CHAR, 0, MPI_COMM_WORLD);

    //Broadcast matrix B to all ranks
    MPI_Bcast(matB[0], matBy * matBx * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);

    //Assign initial load
    RankInfo_t  * rankInfo = new RankInfo_t [commSize];
    MPI_Request * reqList  = new MPI_Request [commSize * 2];
    MPI_Status  * staList  = new MPI_Status [commSize * 2];
    uint offset = 0;
    uint initialLoadSize = 50;
    for (int r=1;r<commSize;r++) {
        rankInfo[r].startRow = offset;
        rankInfo[r].rowCount =  initialLoadSize;
        offset += initialLoadSize;
    }

    offset = 0;
    uint iteration = 0;
    do {
        cout << "[LDIST-" << rank << "]: Iteration " << iteration << endl;
        for (int r=1;r<commSize;r++) 
            cout << "\tRank-" << r << ": " << rankInfo[r].rowCount << endl;

        //Scatter different amount of workload (parts of matrix A) to different ranks
        for (int r=1;r<commSize;r++) {
            //Header
            MPI_Isend(&rankInfo[r].rowCount, sizeof(uint),
                    MPI_CHAR, r, 1, MPI_COMM_WORLD, &reqList[2*(r-1)]);
            //Body
            MPI_Isend(matA[offset], rankInfo[r].rowCount * matAx * sizeof(dtype),
                    MPI_CHAR, r, 2, MPI_COMM_WORLD, &reqList[(2*(r-1))+1]);
            offset += rankInfo[r].rowCount;

            startTimer(&rankInfo[r].tm);
        }
        MPI_Waitall((commSize - 1) * 2, reqList, staList);

        //Gather results back from ranks
        for (int r=1;r<commSize;r++) {
            if (rankInfo[r].rowCount > 0)
                MPI_Irecv(matC[rankInfo[r].startRow], 
                        rankInfo[r].rowCount * matBx * sizeof(dtype),
                        MPI_CHAR, r, 0, MPI_COMM_WORLD, &reqList[r-1]);
        }

        //Wait for all results to arrive
        bool done = true;
        do {
            done = true;
            for (int r=1;r<commSize;r++) {
                if (rankInfo[r].rowCount <= 0)
                    continue;
                int flag;
                MPI_Test(&reqList[r-1], &flag, &staList[r-1]);
                if (flag) {
                    if (isTimerRunning(&rankInfo[r].tm))
                        stopTimer(&rankInfo[r].tm);
                }
                else
                    done = false;
            }
        } while (!done);
        for (int r=1;r<commSize;r++)
            cout << "\tRank-" << r << ": Time = " 
                << getTimerDuration(&rankInfo[r].tm) << " seconds" << endl;

        //Re-calculate workload distribution
#if 1
        uint of = 0;
        double minTime = 1e+9;
        for (int r=1;r<commSize;r++) {
            double diff = getTimerDuration(&rankInfo[r].tm);
            if (diff < minTime)
                minTime = diff;
        }

        for (int r=1;r<commSize;r++) {
            double time = getTimerDuration(&rankInfo[r].tm);
            rankInfo[r].startRow = offset + of;
            if (minTime/time >= 1.0)
                if (r == 1)
                    rankInfo[r].rowCount += 100;
                else 
                    rankInfo[r].rowCount += 2;
            if (minTime/time <= 0.8)
                rankInfo[r].rowCount -= 2;
            if ((rankInfo[r].startRow + rankInfo[r].rowCount) > matAy)
                rankInfo[r].rowCount = matAy - rankInfo[r].startRow;
            of += rankInfo[r].rowCount;
        }
#else
        //Performance-adjusted load distribution (based on execution time of hosts or devices)
        uint of = 0;
        double maxTime = 0.0;
        for (int r=1;r<commSize;r++) {
            double diff = getTimerDuration(&rankInfo[r].tm);
            if (diff > maxTime)
                maxTime = diff;
        }

        double portions = 0.0;
        for (int r=1;r<commSize;r++) {
            double diff = getTimerDuration(&rankInfo[r].tm);
            portions += maxTime / diff;
        }

        for (int r=1;r<commSize;r++) {
            double time = getTimerDuration(&rankInfo[r].tm);
            rankInfo[r].startRow = offset + of;
            rankInfo[r].rowCount = ((maxTime/time)/portions) * (commSize - 1) * initialLoadSize;
            if ((rankInfo[r].startRow + rankInfo[r].rowCount) > matAy)
                rankInfo[r].rowCount = matAy - rankInfo[r].startRow;
            of += rankInfo[r].rowCount;
        }
#endif        

        iteration++;
    } while(offset < matAy);

    //Send complete signal
    uint completeSignal = 0;
    for (int r=1;r<commSize;r++) 
        MPI_Isend(&completeSignal, sizeof(uint), MPI_CHAR, r, 1, MPI_COMM_WORLD, &reqList[r-1]);
    MPI_Waitall(commSize - 1, reqList, staList);

    delete [] rankInfo;
    delete [] reqList;
    delete [] staList;

    //Verify the correctness of the entire multiplication result
    uint failedIdx;
    double failedDiff;
    bool isPassed = verifyMatEqual(matC[0], matCv[0], matCy, matCx, &failedIdx, &failedDiff);
    cout << "Verification: ";
    if (isPassed)
        cout << "PASSED" << endl;
    else
        cout << "FAILED at index [" << failedIdx << "], diff [" << failedDiff << "]" << endl;


}

void computeHandler(int rank, int type) {

    cout << "[COMPUTE-" << rank << "]: Started" << endl;

    //Receive information of matrices
    MatInfo_t matInfo;
    MPI_Bcast(&matInfo, sizeof(MatInfo_t), MPI_CHAR, 0, MPI_COMM_WORLD);

    dtype ** matA, ** matB, **matC;
    uint     matAy=matInfo.matAy, matAx=matInfo.matAx;
    uint     matBy=matInfo.matBy, matBx=matInfo.matBx;
    uint     matCy=matInfo.matCy, matCx=matInfo.matCx;
    createMat(&matA, matAy, matAx);
    createMat(&matB, matBy, matBx);
    createMat(&matC, matCy, matCx);

    //Receive matrix B
    MPI_Bcast(matB[0], matBy * matBx * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);

    MPI_Status  sta;
    bool complete = false;
    uint rowCount = 0;
    do {
        //Receive workload (parts of matrix A)
        MPI_Recv(&rowCount, sizeof(uint), MPI_CHAR, 0, 1, MPI_COMM_WORLD, &sta);
        if (!rowCount)
            break;
        MPI_Recv(matA[0], rowCount * matAx * sizeof(uint), MPI_CHAR, 0, 2, MPI_COMM_WORLD, &sta);

        if (type == 1)
            matmulDevice(matA, rowCount, matAx, matB, matBy, matBx, matC, rowCount, matCx);
        else
            matmulHost(matA, rowCount, matAx, matB, matBy, matBx, matC, rowCount, matCx);

        //Send back result
        MPI_Send(matC[0], rowCount * matBx * sizeof(dtype), MPI_CHAR, 0, 0, MPI_COMM_WORLD);
    } while(!complete);
}

int main(int argc, char ** argv) {

    int rank, commSize;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSize);

    switch (rank) {
    case 0:
        loadDistributor(rank, commSize);
        break;
    case 1:
        computeHandler(rank, 1);
        break;
    default:
        computeHandler(rank, 0);
        break;
    }

    MPI_Finalize();

    return 0;
}

