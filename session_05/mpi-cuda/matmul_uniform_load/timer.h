#ifndef _TIMER_H
#define _TIMER_H

#include <sys/time.h>

typedef struct timeval tv;

typedef struct {
    tv   startTime;
    tv   endTime;
    bool isRunning;
} timer;

void startTimer(timer * tm);
void stopTimer(timer * tm);
double getTimerDuration(timer * tm);
bool isTimerRunning(timer * tm);

#endif /* _TIMER_H */

