#ifndef _COMMON_H
#define _COMMON_H

#include "matbin.h"
#include "timer.h"

typedef struct {
    uint matAy;
    uint matAx;
    uint matBy;
    uint matBx;
    uint matCy;
    uint matCx;
} MatInfo_t;

typedef struct {
    uint        startRow;
    uint        rowCount;
    timer       tm;
} RankInfo_t;

int matmulHost(
    dtype ** matA, uint matAy, uint matAx,
    dtype ** matB, uint matBy, uint matBx,
    dtype ** matC, uint matCy, uint matCx);

int matmulDevice(
    dtype ** matA_h, uint matAy, uint matAx,
    dtype ** matB_h, uint matBy, uint matBx,
    dtype ** matC_h, uint matCy, uint matCx);

#endif /* _COMMON_H */
