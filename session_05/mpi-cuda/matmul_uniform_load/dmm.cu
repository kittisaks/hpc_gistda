#include <cuda_runtime_api.h>

#include <iostream>

#include "matbin.h"

using namespace std;

void transpose(
    dtype ** matt, uint * matty, uint * mattx,
    dtype ** mat,  uint   maty,  uint   matx) {

    for (uint y=0;y<maty;y++) {
        for (uint x=0;x<matx;x++) {
            matt[x][y] = mat[y][x];
        }
    }

    *mattx = maty;
    *matty = matx;
}

void checkError(const char * msg) {
    cudaError_t ce = cudaGetLastError();
    if (ce == cudaSuccess) {
        return;
    }

    cout << "\t" << msg << ": ";
    cout << "FAILED (" << cudaGetErrorString(ce)  << ")" << endl;
    exit(-1);
}

__global__ void matmul_device(
    dtype * matA,  uint ay,  uint ax,
    dtype * matBt, uint bty, uint btx,
    dtype * matC) {

    uint numLocThreads = blockDim.x;
    uint locTid = threadIdx.x;
    uint a_row = blockIdx.x;
    uint a_off = a_row * ax;

    if (a_row >= ay)
        return;

    uint colStride = btx / numLocThreads;
    colStride += (btx % numLocThreads) ? 1 : 0;

    __shared__ dtype locAcc;
    if (locTid == 0)
        locAcc = 0.0;
    __syncthreads();

    for (uint bt_row=0;bt_row<bty;bt_row++) {
        uint bt_off = bt_row * btx;
        uint c_idx = (a_row * bty) + bt_row;


        dtype acc = 0.0;
        for (uint stride=0;stride<colStride;stride++) {
            uint col = ((stride * numLocThreads) + locTid);
           
            uint a_idx = a_off + col;
            uint b_idx = bt_off + col;

            acc += matA[a_idx] * matBt[b_idx];
            
        }
        __syncthreads();

        atomicAdd(&locAcc, acc);
        __syncthreads();

        if (locTid == 0) {
            matC[c_idx] = locAcc;
            locAcc = 0.0;
        }
        __syncthreads();
    }
}

int matmulDevice(
    dtype ** matA_h, uint matAy, uint matAx,
    dtype ** matB_h, uint matBy, uint matBx, 
    dtype ** matC_h, uint matCy, uint matCx) {

    //Transpose the multiplicant
    dtype ** matBt_h;
    uint     matBty = matBx, matBtx = matBy;
    uint     rowt, colt;
    createMat(&matBt_h, matBty, matBtx);
    transpose(matBt_h, &rowt, &colt, matB_h, matBy, matBx);

    //Allocate memory on device
    size_t a_memreq = matAy * matAx * sizeof(dtype);
    size_t bt_memreq = matBty * matBtx * sizeof(dtype);
    size_t c_memreq = matCy * matCx * sizeof(dtype);

    dtype * matA_d, * matBt_d, * matC_d;

    cudaMalloc(&matA_d, a_memreq);
    cudaMalloc(&matBt_d, bt_memreq);
    cudaMalloc(&matC_d, c_memreq);
    cudaMemcpy(matA_d, matA_h[0], a_memreq, cudaMemcpyHostToDevice);
    cudaMemcpy(matBt_d, matBt_h[0], bt_memreq, cudaMemcpyHostToDevice);
    cudaMemset(matC_d, 0, a_memreq);
    checkError("Memory Operation (h->d)");

    matmul_device<<<matAy, 128>>>(matA_d, matAy, matAx, matBt_d, matBty, matBtx, matC_d);
    cudaDeviceSynchronize();
    checkError("Matmul Kernel");

    cudaMemcpy(matC_h[0], matC_d, c_memreq, cudaMemcpyDeviceToHost);
    checkError("Memory Operation (d->h)");

    cudaFree(matA_d);
    cudaFree(matBt_d);
    cudaFree(matC_d);

    destroyMat(matBt_h);

    return 0;
}

