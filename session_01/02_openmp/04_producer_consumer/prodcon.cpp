#include <omp.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <vector>

#define TOTAL_THREADS 8
#define PRODUCER_THREADS 4

using namespace std;

typedef struct {
    int producerIdx;
    int number;
} item_t;

typedef vector<item_t> queue;

void producer(int idx, queue& workQueue) {

    item_t workItem;
    cout << "[PRODUCER-" << idx << "]:" << " Started..." << endl;
    while(1) {
        {
            workItem.producerIdx = idx;
            workItem.number = rand() % 10;
            workQueue.push_back(workItem);
            cout << "[PRODUCER-" << idx << "]:"
                << "Pushed item: " << workItem.number << endl;
        }
        sleep(1);
    }
}

void consumer(int idx, queue& workQueue) {

    item_t workItem;
    cout << "[CONSUMER-" << idx << "]:" << " Started..." << endl;
    while (1) {
        {
            if (workQueue.size() > 0) {
                workItem = workQueue.front();
                workQueue.erase(workQueue.begin());
                cout << "[CONSUMER-" << idx << "]:" << "Retrieved item: " 
                    << workItem.number << " pushed by " << workItem.producerIdx << endl;
            }
        }
        sleep(1);
    }
}

int main(int argc, char ** argv) {
    srand(time(NULL));
    omp_set_num_threads(TOTAL_THREADS);

    queue workQueue;
    workQueue.clear();

    #pragma omp parallel shared(workQueue)
    {
        #pragma omp for schedule (runtime) nowait
        for (int i=0;i<TOTAL_THREADS-PRODUCER_THREADS;i++)
            consumer(i, workQueue);

        #pragma omp for schedule (runtime) nowait
        for (int i=0;i<PRODUCER_THREADS;i++)
            producer(i, workQueue);
    }
}
