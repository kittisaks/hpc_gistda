#include <omp.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define TOTAL_THREADS 6
#define TOTAL_NUMBERS 20000000

typedef int data;

data * numberList;

void initNumberList(data ** nList, unsigned int size) {

    data * nListTemp = new data[size];

    srand(0);
    for (unsigned int i=0;i<size;i++) {
        nListTemp[i] = rand() % 10;
    }

    *nList = nListTemp;
}

data computeSumSingleThread(data * nList, unsigned int size) {

    data acc = 0;
    for (unsigned int i=0;i<size;i++) {
        acc += nList[i];
    }
    return acc;
}


void destroyNumberList(data ** nList) {
    delete [] *nList;
    *nList = NULL;
}

int main(int argc, char ** argv) {

    omp_set_num_threads(TOTAL_THREADS);

    initNumberList(&numberList, TOTAL_NUMBERS);
    data sumSingleThread = computeSumSingleThread(numberList, TOTAL_NUMBERS);
    data sumParallel = 0x00;

    /**
     * Insert OpenMP directive here
     * TODO: Insert your compiler directive here
     */
    for (int idx=0;idx<TOTAL_NUMBERS;idx++) {
        sumParallel += numberList[idx];
    }

    destroyNumberList(&numberList);

    cout << "Summation (Single-Thread): " << sumSingleThread << endl;
    cout << "Summation (Parallel):      " << sumParallel << endl;

    return 0;
}

