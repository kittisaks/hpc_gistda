#include <omp.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

#define ARRAY_SIZE 100000
#define MIN_VALUE 10
#define MAX_VALUE 30000

typedef int dtype;

void generateNumbers(dtype * array, size_t size) {

    srand(1);

    for (size_t i=0;i<size;i++) {
        array[i] = (rand() % MAX_VALUE) + MIN_VALUE;
    }
}

bool verify(dtype * array, size_t size, size_t * index) {
    for (size_t i=0;i<size-1;i++) {
        if (array[i] > array[i+1]) {
            *index = i;
            return false;
        }
    }
    return true;
}

int main(int argc, char ** argv) {

    size_t asize = ARRAY_SIZE;
    dtype * array = new dtype [asize];
    generateNumbers(array, asize);

    /////////////////////////////// SOLUTION /////////////////////////////////

    omp_set_num_threads(2);
    /**
     * Implement Odd-even sort alrogithm here
     * TODO: Insert your code here
     */

    /////////////////////////////////////////////////////////////////////////

    cout << "Verification" << endl;
    size_t index;
    if (verify(array, asize, &index)) {
        cout << "PASSED";
    }
    else {
        cout << "FAILED at index [" << index << "]";
    }
    cout << endl;

    delete [] array;

    return 0;
}

