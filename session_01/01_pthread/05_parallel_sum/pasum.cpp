#include <pthread.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define TOTAL_THREADS 6
#define TOTAL_NUMBERS 200000

typedef int data;

typedef struct {
    pthread_mutex_t * mutex;
    data *            masterSum;
    data *            numberList;
    unsigned int      length;
} threadArgument_t;

data * numberList = NULL;

void initNumberList(data ** nList, unsigned int size) {

    data * nListTemp = new data[size];

    srand(0);
    for (unsigned int i=0;i<size;i++) {
        nListTemp[i] = rand() % 10;
    }

    *nList = nListTemp;
}

void destroyNumberList(data ** nList) {
    delete [] *nList;
    *nList = NULL;
}

data computeSumSingleThread(data * nList, unsigned int size) {

    data acc = 0;
    for (unsigned int i=0;i<size;i++) {
        acc += nList[i];
    }
    return acc;
}

void * computeSumParallel(void * args) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(args);
    pthread_mutex_t * mutex      = arguments->mutex;
    data *            masterSum  = arguments->masterSum;
    data *            nList      = arguments->numberList;
    unsigned int      length     = arguments->length;

    /**
     * Implement the parallel sum algorithm here
     * TODO: Insert your code here
     */

    pthread_exit(NULL);
}

int main(int argc, char ** argv) {

    initNumberList(&numberList, TOTAL_NUMBERS);
    data sumSingleThread = computeSumSingleThread(numberList, TOTAL_NUMBERS);
    data sumParallel = 0x00;

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];
    pthread_mutex_t    thread_mutex;

    //Initialize thread mutual exclusion
    pthread_mutex_init(&thread_mutex, NULL);

    unsigned int elementsPerThread = TOTAL_NUMBERS / TOTAL_THREADS;
    unsigned int remElements       = TOTAL_NUMBERS % TOTAL_THREADS;
    unsigned int startOffset       = 0;
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        pthread_attr_init(thread_attrs + idx);
        pthread_attr_setdetachstate(thread_attrs + idx, PTHREAD_CREATE_JOINABLE);

        unsigned int length = (idx < remElements) ? elementsPerThread + 1 : elementsPerThread;
        thread_args[idx].mutex       = &thread_mutex;
        thread_args[idx].masterSum   = &sumParallel;
        thread_args[idx].numberList  = numberList + startOffset;
        thread_args[idx].length      = length;
        startOffset += length;

        int returnCode = 0x00;
        returnCode =  pthread_create(threads + idx, thread_attrs + idx, computeSumParallel, (void *) &thread_args[idx]);
        if (returnCode) {
            cout << "Cannot create threads  .  .  ." << endl;
            exit(-1);
        }
    }

    //Join threads and release resources
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        void * ret;
        pthread_join(threads[idx], &ret);
        pthread_attr_destroy(thread_attrs + idx);
    }

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;
    destroyNumberList(&numberList);

    cout << "Summation (Single-Thread): " << sumSingleThread << endl;
    cout << "Summation (Parallel):      " << sumParallel << endl;

    return 0;
}

