#include <pthread.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

#define TOTAL_THREADS 6
#define ARRAY_SIZE 100000
#define MIN_VALUE 10
#define MAX_VALUE 30000

typedef int dtype;

typedef struct {
    pthread_barrier_t * barr;
    pthread_mutex_t   * mutex;
    dtype *             array;
    size_t              asize;
    size_t              offset;
    size_t              range;
    bool *              swap;
    int                 threadId;
} threadArgument_t;

void generateNumbers(dtype * array, size_t size) {

    srand(1);

    for (size_t i=0;i<size;i++) {
        array[i] = (rand() % MAX_VALUE) + MIN_VALUE;
    }
}

bool verify(dtype * array, size_t size, size_t * index) {
    for (size_t i=0;i<size-1;i++) {
        if (array[i] > array[i+1]) {
            *index = i;
            return false;
        }
    }
    return true;
}

void * parallelOddEvenSort(void * args) {

    threadArgument_t *  arguments = reinterpret_cast<threadArgument_t *>(args);
    pthread_barrier_t * barr = arguments->barr;
    pthread_mutex_t *   mutex = arguments->mutex;
    dtype *             array = arguments->array;
    size_t              asize = arguments->asize;
    size_t              range = arguments->range;
    size_t              offset = arguments->offset;
    bool *              swap = arguments->swap;
    int                 threadId = arguments->threadId;

    pthread_mutex_lock(mutex);
    cout << "[Thread-" << threadId << "]: Offset=" << offset << " Range=" << range << endl;
    pthread_mutex_unlock(mutex);

    do {
        /**
         * Implement the parallel odd-even sort algorithm here
         * TODO: Insert your code here
         */


    } while(*swap);

    pthread_exit(NULL);
}

int main(int argc, char ** argv) {

    //Initialize unsorted array
    size_t asize = ARRAY_SIZE;
    dtype * array = new dtype [asize];
    generateNumbers(array, asize);

    bool swap = false;

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];

    pthread_barrier_t barr;
    pthread_barrier_init(&barr, NULL, TOTAL_THREADS);
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    //Create threads with their corresponding arguments
    size_t defRange = asize / TOTAL_THREADS;
    size_t remRange = asize % TOTAL_THREADS;
    size_t offset = 0;
    size_t defRangeAddition = 0; 
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        if (remRange > 0)
            defRangeAddition = (idx < remRange) ? 1 : 0;

        pthread_attr_init(thread_attrs + idx);
        pthread_attr_setdetachstate(thread_attrs + idx, PTHREAD_CREATE_JOINABLE);
        thread_args[idx].barr = &barr;
        thread_args[idx].mutex = &mutex;
        thread_args[idx].array = array;
        thread_args[idx].asize = asize;
        thread_args[idx].range = defRange + defRangeAddition;
        thread_args[idx].offset = offset;
        thread_args[idx].swap = &swap;
        thread_args[idx].threadId = idx;

        offset += thread_args[idx].range;

        int returnCode = 0x00;
        returnCode =  pthread_create(threads + idx, thread_attrs + idx, parallelOddEvenSort, (void *) &thread_args[idx]);
        if (returnCode) {
            cout << "Cannot create threads  .  .  ." << endl;
            exit(-1);
        }
    }

    //Join threads and release resources
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        void * ret;
        pthread_join(threads[idx], &ret);
        pthread_attr_destroy(thread_attrs + idx);
    }

    cout << "Verification" << endl;
    size_t index;
    if (verify(array, asize, &index)) {
        cout << "PASSED";
    }
    else {
        cout << "FAILED at index [" << index << "]";
    }
    cout << endl;

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;
    delete [] array;


    return 0;
}

