#include <pthread.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>
#include <vector>

using namespace std;

#define TOTAL_THREADS 6
#define PRODUCER_THREADS 2

typedef struct {
    int idx;
    int item;
} item_t;

typedef vector<item_t> queue;

typedef struct {
    int               idx;
    queue *           workQueue;
    pthread_mutex_t * mutex;
} threadArgument_t;

void * producer(void * args) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(args);
    int                idx       = arguments->idx;
    queue *            workQueue = arguments->workQueue;
    pthread_mutex_t *  mutex     = arguments->mutex;

    /**
     * Implement the routine of the producers here
     * TODO: Insert your code here
     */

    pthread_exit(NULL);
}

void * consumer(void * args) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(args); 
    int                idx       = arguments->idx;
    queue *            workQueue = arguments->workQueue;
    pthread_mutex_t *  mutex     = arguments->mutex;

    /**
     * Implement the routine of the consumers here
     * TODO: Insert your oode here
     */

    pthread_exit(NULL);
}

int main(int argc, char ** argv) {

    srand(time(NULL));

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];
    pthread_mutex_t    thread_mutex;

    queue workQueue;
    workQueue.clear();

    //Initialize thread mutual exclusion
    pthread_mutex_init(&thread_mutex, NULL);

    //Initialize resources and threads
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        pthread_attr_init(thread_attrs + idx);
        pthread_attr_setdetachstate(thread_attrs + idx, PTHREAD_CREATE_JOINABLE);

        //Determine whether the thread being created should be a PRODUCER or a CONSUMER
        void * (* threadFunc)(void *) = (idx < PRODUCER_THREADS) ? producer : consumer;

        //Set the correct set of arguments for each of the threads
        threadArgument_t * args = &thread_args[idx];
        args->idx       = idx;
        args->workQueue = &workQueue;
        args->mutex     = &thread_mutex;

        int returnCode = 0x00;
        returnCode =  pthread_create(threads + idx, thread_attrs + idx, threadFunc, (void *) args);
        if (returnCode) {
            cout << "Cannot create threads  .  .  ." << endl;
            exit(-1);
        }
    }

    //Join threads and release resources
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        void * ret;
        pthread_join(threads[idx], &ret);
        pthread_attr_destroy(thread_attrs + idx);
    }

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;

    return 0;
}
