#include <pthread.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define TOTAL_THREADS 6
#define TOTAL_NUMBERS 200000

typedef int data;

typedef struct {
    int threadId;
} threadArgument_t;

void * threadFunc(void * args) {

    threadArgument_t * casted_arg = (threadArgument_t *) args;
    int threadId = casted_arg->threadId;

    cout << "Hello I am thread [" << threadId << "]"  << endl;

}

int main(int argc, char ** argv) {

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];

    /**
     * You will need to create threads here
     * TODO: Insert your code here
     */
    for (int i=0;i<TOTAL_THREADS;i++) {
	pthread_attr_init(&thread_attrs[i]);
	pthread_attr_setdetachstate(&thread_attrs[i], PTHREAD_CREATE_JOINABLE);

	thread_args[i].threadId = i;

    	pthread_create(&threads[i], &thread_attrs[i], threadFunc, &thread_args[i]);
    }

    
    /**
     * You will need to join the threads here
     * TODO: Insert your code here
     */
    void * ret;
    for (int i=0;i<TOTAL_THREADS;i++) {
        pthread_join(threads[i], &ret);
    }

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;

    return 0;
}

