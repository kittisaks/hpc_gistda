#!/bin/bash

pid_condv_poll=$(pgrep -f poll_wait)
pid_condv=$(pgrep -f condv_wait)

pid_all=
if [ ! -z "$pid_condv" ]
then
    pid_all=$pid_all" -p $pid_condv"
fi
if [ ! -z "$pid_condv_poll" ]
then
    pid_all=$pid_all" -p $pid_condv_poll"
fi
if [ -z "$pid_all" ]
then
    echo No conditional variable or polling exmaples found running.
    exit
fi

top -d 0.5 $pid_all
