#include <pthread.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#define TOTAL_THREADS 6
#define TOTAL_NUMBERS 200000

typedef int data;

typedef struct {
    pthread_mutex_t * mutex;
    int               threadId;
} threadArgument_t;

void * threadFunc(void * args) {

    /**
     * Write the code to print pretty Hello World! message using MUTEX
     * TODO: Insert your code here
     */

}

int main(int argc, char ** argv) {

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];

    /**
     * Initialize the mutex here
     * TODO: Insert your code here
     */


    /**
     * Create threads and pass appropriate MUTEX and thread ID here
     * TODO: Insert your code here
     */

    //Join threads and release resources
    /**
     *  Join the threads here
     *  TODO: Insert your code here
     */

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;

    return 0;
}

