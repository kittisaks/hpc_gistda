/**
 * Author: Kittisak Sajjapongse
 * Email:  ks5z9@mail.missouri.edu
 */

#include <omp.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "matbin.h"

using namespace std;

#define DATASET_PATH   "../../dataset/matmul_dset/Y15000_X15000_00/"
#define MAT_A_FILENAME "matA_Y15000_X15000.bin"
#define MAT_B_FILENAME "matB_Y15000_X15000.bin"
#define MAT_C_FILENAME "matC_Y15000_X15000.bin"

int readMatrixFromBinaryFile(
    const char * filename, dtype *** mat, 
    unsigned int * dimY, unsigned int * dimX) {

    char filePath[128];
    strcpy(filePath, DATASET_PATH);
    strcat(filePath, filename);
    return readMatBinaryFile(filePath, mat, dimY, dimX);
}

int handleError(const char * msg) {
    cerr << "[Error]: " << msg << endl;
    exit(-1);
}

int compareMat(
    dtype ** m1, unsigned int m1DimY, unsigned int m1DimX,
    dtype ** m2, unsigned int m2DimY, unsigned int m2DimX
) {

    if (m1DimY != m2DimY)
        return -1;
    if (m1DimX != m2DimX)
        return -1;

    for (unsigned int i=0;i<m1DimY;i++) {
        for (unsigned int j=0;j<m2DimY;j++) {
            if (m1[i][j] != m2[i][j]) {
                cout << "(" << i << ", " << j << "):" << m1[i][j] << " / " << m2[i][j] << endl;
                return -1;
            }
        }
    }

    return 0;
}


#define CHECK(str, errMsg)\
    do {\
        int ret = str;\
        if (ret) {\
            handleError(errMsg);\
        }\
    } while (0);\

int main(int argc, char ** argv) {

    dtype ** ma, ** mb, ** mc ,** mcv;
    unsigned int aY, aX, bY, bX, cY, cX, cvY, cvX;

    /* Load matrices from files */
    CHECK(readMatrixFromBinaryFile(MAT_A_FILENAME, &ma, &aX, &aY),
        "Cannot load matrix-A from file.");
    CHECK(readMatrixFromBinaryFile(MAT_B_FILENAME, &mb, &bX, &bY),
        "Cannot load matrix-B from file");
    CHECK(readMatrixFromBinaryFile(MAT_C_FILENAME, &mcv, &cvX, &cvY),
        "Cannot load matrix-C (verification) from file");
    if (aX != bY)
        handleError("Dimension of matrix-A and matrix-B do not match");

    /* Allocate memory for the result matrix-C*/
    cY = aY; cX = bX;
    CHECK(createMat(&mc, cY, cX), 
        "Cannot allocate memory for result (matrix-C)");
    memset(reinterpret_cast<void *>(mc[0]), 0, aY * bX * sizeof(dtype));

    /* Display information of matrices */
    cout << endl << endl << endl;
    cout << "Matrix (A): Dimension (x,y): (" << aX << ", " << aY << ")" << endl;
    cout << "Matrix (B): Dimension (x,y): (" << bX << ", " << bY << ")" << endl;
    cout << "Matrix (C)  Dimension (x,y): (" << cX << ", " << cY << ")" << endl;
    cout << "Matrix (C) for verification: Dimension (x,y): (" << cvX << ", " << cvY << ")" << endl;

    //////////////////////////////// SOLUTION ////////////////////////////////
    omp_set_num_threads(24);

    #pragma omp parallel for shared (ma, mb, mc)
    for (unsigned int i=0;i<cY;i++) {
        for (unsigned int j =0;j<cX;j++) {
            dtype acc = 0.0;
            for (unsigned int k=0;k<aX;k++) {
                acc += ma[i][k] * mb[k][j];
            }
            mc[i][j] = acc;
        }
    }

    //////////////////////////////////////////////////////////////////////////

    int isNotEqual = compareMat(mc, cY, cX, mcv, cvY, cvX);
    cout << "[Verification]: ";
    if (isNotEqual)
        cout << "FAILED";
    else
        cout << "PASSED";

    cout << endl << endl << endl;
    return 0;
}

