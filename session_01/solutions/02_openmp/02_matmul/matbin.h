/**
 * Author: Kittisak Sajjapongse
 * Email:  ks5z9@mail.missouri.edu
 */

#ifndef _MATBIN_H
#define _MATBIN_H

#define MATRIX_DATATYPE float
typedef MATRIX_DATATYPE dtype;

int createMat(dtype *** mat, unsigned int dimY, unsigned int dimX);

int destroyMat(dtype ** mat);

int writeMatBinaryFile(const char * filename, dtype ** mat, unsigned int dimY, unsigned int dimX);

int readMatBinaryFile(const char * filename, dtype *** mat, unsigned int * dimY, unsigned int * dimX);

#endif /* _MATBIN_H */

