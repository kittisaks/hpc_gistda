/**
 * Author: Kittisak Sajjapongse
 * Email:  ks5z9@mail.missouri.edu
 */

#include <omp.h>
#include <stdlib.h>

#include <iostream>

using namespace std;

#define ARRAY_SIZE 100000
#define MIN_VALUE 10
#define MAX_VALUE 30000

typedef int dtype;

void generateNumbers(dtype * array, size_t size) {

    srand(1);

    for (size_t i=0;i<size;i++) {
        array[i] = (rand() % MAX_VALUE) + MIN_VALUE;
    }
}

bool verify(dtype * array, size_t size, size_t * index) {
    for (size_t i=0;i<size-1;i++) {
        if (array[i] > array[i+1]) {
            *index = i;
            return false;
        }
    }
    return true;
}

int main(int argc, char ** argv) {

    size_t asize = ARRAY_SIZE;
    dtype * array = new dtype [asize];
    generateNumbers(array, asize);


    omp_set_num_threads(4);

    bool swap = false;
    do {
        {
            swap = false;
        }
        for (size_t p=0;p<2;p++) {
            #pragma omp parallel shared(array, swap)
            #pragma omp for schedule(static, 4)
            for (size_t idx=p;idx<asize;idx+=2) {
                if (idx > asize - 2)
                    continue;
                if (array[idx] > array[idx+1]) {
                    dtype temp = array[idx];
                    array[idx] = array[idx+1];
                    array[idx+1] = temp;
                    swap = true;
                }
            }
        }
    } while (swap);


    cout << "Verification" << endl;
    size_t index;
    if (verify(array, asize, &index)) {
        cout << "PASSED";
    }
    else {
        cout << "FAILED at index [" << index << "]";
    }
    cout << endl;

    delete [] array;

    return 0;
}

