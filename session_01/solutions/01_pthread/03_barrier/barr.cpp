/**
 * Author: Kittisak Sajjapongse
 * Email:  ks5z9@mail.missouri.edu
 */

#include <pthread.h>

#include <iostream>

using namespace std;

#define TOTAL_THREADS 10

typedef struct {
    pthread_barrier_t * barr;
    pthread_mutex_t   * mutex;
    int                 threadId;
} threadArgument_t;

void * threadFunc(void * args) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(args);
    pthread_barrier_t * barr = arguments->barr;
    pthread_mutex_t * mutex = arguments->mutex;
    int threadId = arguments->threadId;

#define _ENABLE_BARRIER

    pthread_mutex_lock(mutex);
    cout << "[Thread-" << threadId << "]: Message 1" << endl;
    pthread_mutex_unlock(mutex);
#ifdef _ENABLE_BARRIER
    pthread_barrier_wait(barr);
#endif
    pthread_mutex_lock(mutex);
    cout << "[Thread-" << threadId << "]: Message 2" << endl;
    pthread_mutex_unlock(mutex);
#ifdef _ENABLE_BARRIER
    pthread_barrier_wait(barr);
#endif
    pthread_mutex_lock(mutex);
    cout << "[Thread-" << threadId << "]: Message 3" << endl;
    pthread_mutex_unlock(mutex);
#ifdef _ENABLE_BARRIER
    pthread_barrier_wait(barr);
#endif
    pthread_mutex_lock(mutex);
    cout << "[Thread-" << threadId << "]: Message 4" << endl;
    pthread_mutex_unlock(mutex);
#ifdef _ENABLE_BARRIER
    pthread_barrier_wait(barr);
#endif

    pthread_exit(NULL);
}

int main(int argc, char ** argv) {

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];

    pthread_barrier_t barr;
    pthread_barrier_init(&barr, NULL, TOTAL_THREADS);
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);

    //Create threads with their corresponding arguments
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        pthread_attr_init(thread_attrs + idx);
        pthread_attr_setdetachstate(thread_attrs + idx, PTHREAD_CREATE_JOINABLE);
        thread_args[idx].barr = &barr;
        thread_args[idx].mutex = &mutex;
        thread_args[idx].threadId = idx;

        int returnCode = 0x00;
        returnCode =  pthread_create(threads + idx, thread_attrs + idx, threadFunc, (void *) &thread_args[idx]);
        if (returnCode) {
            cout << "Cannot create threads  .  .  ." << endl;
            exit(-1);
        }
    }

    //Join threads and release resources
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        void * ret;
        pthread_join(threads[idx], &ret);
        pthread_attr_destroy(thread_attrs + idx);
    }

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;


    return 0;
}

