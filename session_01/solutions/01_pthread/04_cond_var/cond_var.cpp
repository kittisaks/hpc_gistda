/**
 * Author: Kittisak Sajjapongse
 * Email:  ks5z9@mail.missouri.edu
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

using namespace std;

#define TOTAL_THREADS 10

typedef struct {
    int                idx;
    pthread_mutex_t *  mutex;
    pthread_cond_t *   condv;
}threadArgument_t;

void * slave(void * arg) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(arg);
    int               idx   = arguments->idx;
    pthread_mutex_t * mutex = arguments->mutex;
    pthread_cond_t *  condv = arguments->condv;

    pthread_mutex_lock(mutex);
    cout << "[SLAVE-" << idx << "]: Waiting for signal on conditional variable  .  .  ." << endl;
    pthread_mutex_unlock(mutex);

    pthread_mutex_lock(mutex);
    pthread_cond_wait(condv, mutex);
    cout << "[SLAVE-" << idx << "]: Continue  .  .  ." << endl;
    pthread_mutex_unlock(mutex);

    pthread_exit(NULL);
}

void * master(void * arg) {

    threadArgument_t * arguments = reinterpret_cast<threadArgument_t *>(arg);
    int               idx   = arguments->idx;
    pthread_mutex_t * mutex = arguments->mutex;
    pthread_cond_t *  condv = arguments->condv;

    pthread_mutex_lock(mutex);
    cout << "[MASTER-" << idx << "]: Master started. Press any key to notify slaves." << endl;
    pthread_mutex_unlock(mutex);
    
    getchar();
    pthread_mutex_lock(mutex);
    cout << "[MASTER-" << idx << "]: Master sends signal to slaves  .  .  ." << endl;
    pthread_cond_broadcast(condv);
    pthread_mutex_unlock(mutex);

    pthread_exit(NULL);

}

int main(int argc, char ** argv) {

    pthread_t *        threads      = new pthread_t[TOTAL_THREADS];
    pthread_attr_t *   thread_attrs = new pthread_attr_t[TOTAL_THREADS];
    threadArgument_t * thread_args  = new threadArgument_t[TOTAL_THREADS];
    pthread_mutex_t    mutex;
    pthread_cond_t     condv;

    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&condv, NULL);

    //Initialize resources and threads
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        pthread_attr_init(thread_attrs + idx);
        pthread_attr_setdetachstate(thread_attrs + idx, PTHREAD_CREATE_JOINABLE);

        //Determine whether the thread being created should be a PRODUCER or a CONSUMER
        void * (* threadFunc)(void *) = (idx == 0) ? master : slave;

        //Set the correct set of arguments for each of the threads
        threadArgument_t * args = &thread_args[idx];
        args->idx       = idx;
        args->mutex     = &mutex;
        args->condv     = &condv;

        int returnCode = 0x00;
        returnCode =  pthread_create(threads + idx, thread_attrs + idx, threadFunc, (void *) args);
        if (returnCode) {
            cout << "Cannot create threads  .  .  ." << endl;
            exit(-1);
        }
    }

    //Join threads and release resources
    for (int idx=0;idx<TOTAL_THREADS;idx++) {

        void * ret;
        pthread_join(threads[idx], &ret);
        pthread_attr_destroy(thread_attrs + idx);
    }

    pthread_cond_destroy(&condv);
    pthread_mutex_destroy(&mutex);

    delete [] thread_args;
    delete [] thread_attrs;
    delete [] threads;

    return 0;
}
