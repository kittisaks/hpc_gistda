#include <cuda_runtime_api.h>
#include <math.h>

#include <iostream>

#include "nbbin.h"

#define DATASET_FILE "nb_100000_100.bin"
#define G_CONST 6.67e-6
#define MASS 1.0

using namespace std;

void allocateSnapshotHost(nbSnapshot_t * ss, uint32_t bodyCnt) {
    ss->posX = new dtype [bodyCnt];
    ss->posY = new dtype [bodyCnt];
    ss->posZ = new dtype [bodyCnt];
    ss->velX = new dtype [bodyCnt];
    ss->velY = new dtype [bodyCnt];
    ss->velZ = new dtype [bodyCnt];
    ss->accX = new dtype [bodyCnt];
    ss->accY = new dtype [bodyCnt];
    ss->accZ = new dtype [bodyCnt];
}

void allocateSnapshotDevice(nbSnapshot_t * ss, uint32_t bodyCnt) {
    size_t memReq = bodyCnt * sizeof(dtype);

    cudaMalloc(&ss->posX, memReq);
    cudaMalloc(&ss->posY, memReq);
    cudaMalloc(&ss->posZ, memReq);
    cudaMalloc(&ss->velX, memReq);
    cudaMalloc(&ss->velY, memReq);
    cudaMalloc(&ss->velZ, memReq);
    cudaMalloc(&ss->accX, memReq);
    cudaMalloc(&ss->accY, memReq);
    cudaMalloc(&ss->accZ, memReq);
}

void copySnapshot(nbSnapshot_t dst, nbSnapshot_t src, uint32_t bodyCnt, cudaMemcpyKind direction) {
    size_t memReq = bodyCnt * sizeof(dtype);

    cudaMemcpy(dst.posX, src.posX, memReq, direction);
    cudaMemcpy(dst.posY, src.posY, memReq, direction);
    cudaMemcpy(dst.posZ, src.posZ, memReq, direction);
    cudaMemcpy(dst.velX, src.velX, memReq, direction);
    cudaMemcpy(dst.velY, src.velY, memReq, direction);
    cudaMemcpy(dst.velZ, src.velZ, memReq, direction);
    cudaMemcpy(dst.accX, src.accX, memReq, direction);
    cudaMemcpy(dst.accY, src.accY, memReq, direction);
    cudaMemcpy(dst.accZ, src.accZ, memReq, direction);
}

bool verifySnapshotEqual(nbSnapshot_t ss, nbSnapshot_t ref, uint32_t failedIdx, uint32_t failedDiff) {

    return true;
}

void checkError(const char * msg) {
    cout << msg << ": ";
    cudaError_t ce = cudaGetLastError();
    if (ce == cudaSuccess) {
        cout << "PASSED" << endl;
        return;
    }

    cout << "FAILED (" << cudaGetErrorString(ce)  << ")" << endl;
    exit(-1);
}

__global__ void nbody_device(nbSnapshot_t ssc, nbSnapshot_t ssn, uint32_t bodyCnt, dtype timeDelta) {

    //TODO: Implement your kernel here

}

inline bool pDiff(uint32_t idx, dtype val, dtype ref) {
    double diff = abs((double)val - (double)ref) / (double) ref;
    if (diff > 0.01) {
        cout << idx << " " << val << " / " << ref << " / " << diff << endl;
        return false;
    }
    return true;
}

bool verifyEpochEqual(nbSnapshot_t * ss, nbSnapshot_t * ref_ss, uint32_t bodyCnt, uint32_t * fIdx) {

    for (uint32_t idx=0;idx<bodyCnt;idx++) {
        if (!pDiff(idx, ss->posX[idx], ref_ss->posX[idx]))
            return false;
        if (!pDiff(idx, ss->posY[idx], ref_ss->posY[idx]))
            return false;
        if (!pDiff(idx, ss->posZ[idx], ref_ss->posZ[idx]))
            return false;
    }

    return true;
}

int main(int argc, char ** argv) {

    cout << "Starting N-body " << endl;
    nbDataset_t dset = nbLoadDatasetFromFile(DATASET_FILE);
    cout << "Data Loaded" << endl;
    uint32_t bodyCnt   = dset->fileHeader.bodyCnt;
    uint32_t ssCnt     = dset->fileHeader.snapshotCnt;
    dtype    timeDelta = dset->fileHeader.timeDelta;
    cout << "Body count:     " << bodyCnt << endl;
    cout << "Snapshot count: " << ssCnt << endl;
    cout << "Time delta:     " << timeDelta << endl;

    //Challenge: where does ss_d and its members reside/points to?
    nbSnapshot_t ss_h, ssc_d, ssn_d;
    allocateSnapshotHost(&ss_h, bodyCnt);
    allocateSnapshotDevice(&ssc_d, bodyCnt);
    allocateSnapshotDevice(&ssn_d, bodyCnt);
    copySnapshot(ssc_d, *(dset->snapshots->at(0)), bodyCnt, cudaMemcpyHostToDevice);
    checkError("Memory Operations (h->d)");

    for (uint32_t epoch=1;epoch<6;epoch++) {
        dim3 grid(1000, 100, 1);
        nbody_device<<<grid, 128>>>(ssc_d, ssn_d, bodyCnt, timeDelta);
        cudaDeviceSynchronize();
        checkError("\tKernel Execution");
        copySnapshot(ss_h, ssn_d, bodyCnt, cudaMemcpyDeviceToHost);
        checkError("\tResult Copying (d->h)");
        copySnapshot(ssc_d, ssn_d, bodyCnt, cudaMemcpyDeviceToDevice);
        checkError("\tResult Copying (d->d)");
        uint32_t fIdx;
        bool isPassed = verifyEpochEqual(&ss_h, dset->snapshots->at(epoch), bodyCnt, &fIdx);
        if (isPassed)
            cout << "Epoch[" << epoch << "]: PASSED" << endl;
        else {
            cout << "Epoch[" << epoch << "]: FAILED" << endl;
            return -1;
        }
    }

    return 0;
}

