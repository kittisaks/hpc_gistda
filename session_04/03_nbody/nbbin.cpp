#include <string.h>

#include <fstream>
#include <iostream>
#include <vector>

#include "nbbin.h"

using namespace std;

typedef vector<nbSnapshot_t *> snapshot_list;
typedef snapshot_list::iterator snapshot_iter;
typedef vector<nbSnapshotHeader_t> ssHeader_list;
typedef ssHeader_list::iterator ssHeader_iter;

/* @API */
nbDataset_t nbInitializeDataset(const char * filename, uint32_t bodyCnt, dtype timeDelta) {

    nbDataset_t dataset = new nbDataset_inst_t();
    dataset->snapshots = new snapshot_list();
    dataset->snapshotHeaders = new ssHeader_list();
    dataset->fileName = const_cast<char *>(filename);
    dataset->fileHeader.bodyCnt = bodyCnt;
    dataset->fileHeader.timeDelta = timeDelta;
    dataset->fileHeader.snapshotCnt = 0;

    return dataset;
}

#define INIT_SS_MEMBER(snapshot, member, bodyCnt) \
    snapshot->member = new dtype [bodyCnt]; \
    memset(reinterpret_cast<void *>(snapshot->member), 0, bodyCnt * sizeof(dtype))

nbSnapshot_t * createEmptySnapshot(uint32_t bodyCnt) {
    nbSnapshot_t * ssTemp = new nbSnapshot_t();
    INIT_SS_MEMBER(ssTemp, posX, bodyCnt);
    INIT_SS_MEMBER(ssTemp, posY, bodyCnt);
    INIT_SS_MEMBER(ssTemp, posZ, bodyCnt);
    INIT_SS_MEMBER(ssTemp, velX, bodyCnt);
    INIT_SS_MEMBER(ssTemp, velY, bodyCnt);
    INIT_SS_MEMBER(ssTemp, velZ, bodyCnt);
    INIT_SS_MEMBER(ssTemp, accX, bodyCnt);
    INIT_SS_MEMBER(ssTemp, accY, bodyCnt);
    INIT_SS_MEMBER(ssTemp, accZ, bodyCnt);

    return ssTemp;
}
#undef INIT_SS_MEMBER

#define COPY_SS_MEMBER(dst, src, member, bodyCnt) \
    memcpy(dst->member, src->member, bodyCnt * sizeof(dtype))

nbSnapshot_t * copySnapshot(nbSnapshot_t * src, uint32_t bodyCnt) {
    nbSnapshot_t * dst = createEmptySnapshot(bodyCnt);
    if (dst == NULL)
        return NULL;

    COPY_SS_MEMBER(dst, src, posX, bodyCnt);
    COPY_SS_MEMBER(dst, src, posY, bodyCnt);
    COPY_SS_MEMBER(dst, src, posZ, bodyCnt);
    COPY_SS_MEMBER(dst, src, velX, bodyCnt);
    COPY_SS_MEMBER(dst, src, velY, bodyCnt);
    COPY_SS_MEMBER(dst, src, velZ, bodyCnt);
    COPY_SS_MEMBER(dst, src, accX, bodyCnt);
    COPY_SS_MEMBER(dst, src, accY, bodyCnt);
    COPY_SS_MEMBER(dst, src, accZ, bodyCnt);

    return dst;
}
#undef COPY_SS_MEMBER

void destroySnapshot(nbSnapshot_t * snapshot) {
    delete [] snapshot->posX;
    delete [] snapshot->posY;
    delete [] snapshot->posZ;
    delete [] snapshot->velX;
    delete [] snapshot->velY;
    delete [] snapshot->velZ;
    delete [] snapshot->accX;
    delete [] snapshot->accY;
    delete [] snapshot->accZ;
    delete snapshot;
}

/* @API */
int nbDataset_AddSnapshot(nbDataset_t dataset, dtype timestamp, nbSnapshot_t * snapshot) {

    //TODO: Check if timestamp is multiple of timeDelta

    uint32_t bodyCnt = dataset->fileHeader.bodyCnt;
    nbSnapshot_t * snapshotCopy = copySnapshot(snapshot, bodyCnt);
    if (snapshotCopy == NULL)
        return -1;

    nbSnapshotHeader_t ssHeader;
    ssHeader.timestamp = timestamp;

    dataset->snapshots->push_back(snapshotCopy);
    dataset->snapshotHeaders->push_back(ssHeader);
    dataset->fileHeader.snapshotCnt += 1;

    return 0;
}

/* @API */
int nbFinalize(nbDataset_t dataset) {

    uint32_t bodyCnt = dataset->fileHeader.bodyCnt;
    uint32_t snapshotCnt = dataset->snapshots->size();
    uint32_t snapshotHeaderCnt = dataset->snapshotHeaders->size();

    if (snapshotCnt != snapshotHeaderCnt)
        return -2;

    fstream file;

    file.open(dataset->fileName, ios::out | ios::binary);
    if (!file.is_open())
        return -3;

    file.write(reinterpret_cast<char *>(&dataset->fileHeader), sizeof(nbFileHeader_t));

    ssHeader_list * ssHeaderList = dataset->snapshotHeaders;
    for (ssHeader_iter it=ssHeaderList->begin();it!=ssHeaderList->end();++it) {
        nbSnapshotHeader_t header = *it;
        file.write(reinterpret_cast<char *>(&header), sizeof(nbSnapshotHeader_t));
    }

    snapshot_list * ssList = dataset->snapshots;
    for (snapshot_iter it=ssList->begin();it!=ssList->end();++it) {
        nbSnapshot_t * ss = *it;
        file.write(reinterpret_cast<char *>(ss->posX), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->posY), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->posZ), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->velX), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->velY), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->velZ), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->accX), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->accY), bodyCnt * sizeof(dtype));
        file.write(reinterpret_cast<char *>(ss->accZ), bodyCnt * sizeof(dtype));
    }

    file.close();

    return 0;
}

/* @API */
void nbDestroyDataset(nbDataset_t dataset) {

    snapshot_list * ssList = dataset->snapshots;
    for (snapshot_iter it=ssList->begin();it!=ssList->end();++it)
        destroySnapshot(*it);

    delete dataset->snapshots;
    delete dataset->snapshotHeaders;
    delete dataset;
}

/* @API */
nbDataset_t nbLoadDatasetFromFile(const char * filename) {

    nbDataset_t dataset = nbInitializeDataset(filename, 0, 0.1);

    fstream file;
    
    file.open(dataset->fileName, ios::in | ios::binary);
    if (!file.is_open())
        return NULL;

    file.read(reinterpret_cast<char *>(&dataset->fileHeader), sizeof(nbFileHeader_t));

#if 0
    cout << "=== READ ===" << endl;
    cout << "bodyCnt: " << dataset->fileHeader.bodyCnt << endl;
    cout << "timeDelta: " << dataset->fileHeader.timeDelta << endl;
    cout << "snapshotCnt: " << dataset->fileHeader.snapshotCnt << endl;
#endif

    uint32_t snapshotCnt = dataset->fileHeader.snapshotCnt;
    ssHeader_list * ssHeaders = dataset->snapshotHeaders;
    for (uint32_t ssIdx=0;ssIdx<snapshotCnt;ssIdx++) {
        nbSnapshotHeader_t ssHeader;
        file.read(reinterpret_cast<char *>(&ssHeader), sizeof(nbSnapshotHeader_t));
        ssHeaders->push_back(ssHeader);
#if 0        
        cout << "=== ssHeader " << ssIdx << " ===" << endl;
        cout << "timestamp: " << ssHeader.timestamp << endl;
#endif
    }

    uint32_t bodyCnt = dataset->fileHeader.bodyCnt;
    snapshot_list * snapshots = dataset->snapshots;
    for (uint32_t ssIdx=0;ssIdx<snapshotCnt;ssIdx++) {
        nbSnapshot_t * ss = createEmptySnapshot(bodyCnt);
        file.read(reinterpret_cast<char *>(ss->posX), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->posY), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->posZ), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->velX), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->velY), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->velZ), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->accX), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->accY), bodyCnt * sizeof(dtype));
        file.read(reinterpret_cast<char *>(ss->accZ), bodyCnt * sizeof(dtype));
        snapshots->push_back(ss);
        
#if 0
        cout << "=== ss " << ssIdx << " ===" << endl;
#endif
    }

    file.close();


    return dataset;
}


