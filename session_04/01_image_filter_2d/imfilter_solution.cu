#include "matbin.h"

#include <stdlib.h>

#include <iostream>

#define IMAGE_FILE "image.bin"
#define FILTER_FILE "filter.bin"

using namespace std;

void checkError(const char * msg) {
    cout << msg << ": ";
    cudaError_t ce = cudaGetLastError();
    if (ce == cudaSuccess) {
        cout << "PASSED" << endl;
        return;
    }

    cout << "FAILED (" << cudaGetErrorString(ce)  << ")" << endl;
    exit(-1);
}

void applyFilter_host(
        dtype * image,    uint image_dy,  uint image_dx,
        dtype * filter,   uint filter_dy, uint filter_dx,
        dtype * image_out) {

    uint fCenter_y = filter_dy / 2;
    uint fCenter_x = filter_dx / 2;

    cout << "Filter center-y: " << fCenter_y << endl;
    cout << "Filter center-x: " << fCenter_x << endl;

    for (uint y=0;y<image_dy;y++) {
        for (uint x=0;x<image_dx;x++) {

//            cout << "y[" << y << "] x[" << x << "] ";

            dtype acc = 0.0;
            for (uint fy=0;fy<filter_dy;fy++) {
                for (uint fx=0;fx<filter_dx;fx++) {
                    int iy = y + (fy - fCenter_y);
                    int ix = x + (fx - fCenter_x);
                    if ((iy < 0) || (ix < 0))
                        continue;
                    if ((iy >= image_dy) || (ix >= image_dx))
                        continue;
//                    cout << "[" << iy << ", " << ix << "]/[" << fy << ", " << fx << "] ";
                    int filterBlk = (fy * filter_dx) + fx;
                    int imageBlk = (iy * image_dx) + ix;
                    acc += image[imageBlk] * filter[filterBlk];
                }
            }

//            cout << endl;
            int imageOutBlk = (y * image_dx) + x;
            image_out[imageOutBlk] = acc;
        }
    }
}

bool verifyImageEqual(dtype * image, dtype * ref, uint dy, uint dx, uint * failedIdx, uint * failedDiff) {

    uint totalLength = dy * dx;
    for (uint idx=0;idx<totalLength;idx++) {
        uint diff = abs(image[idx] - ref[idx]);
        if (diff > 128) {
            cout << image[idx] << " / " << ref[idx] << endl;
            *failedDiff = diff;
            *failedIdx = idx;
            return false;
        }
    }
    return true;
}

__global__ void applyFilter_device(
        dtype * image,    uint image_dy,  uint image_dx,
        dtype * filter,   uint filter_dy, uint filter_dx,
        dtype * image_out) {

    uint fCenter_y = filter_dy / 2;
    uint fCenter_x = filter_dx / 2;

    uint tid = (blockIdx.x * blockDim.x) + threadIdx.x;

    uint y = tid / image_dx;
    uint x = tid % image_dx;
    if ((y > image_dy) || (x > image_dx))
        return;

    dtype acc = 0.0;
    for (uint fy=0;fy<filter_dy;fy++) {
        for (uint fx=0;fx<filter_dx;fx++) {
            int iy = y + (fy - fCenter_y);
            int ix = x + (fx - fCenter_x);
            if ((iy < 0) || (ix < 0))
                continue;
            if ((iy >= image_dy) || (ix >= image_dx))
                continue;
            int filterBlk = (fy * filter_dx) + fx;
            int imageBlk = (iy * image_dx) + ix;
            acc += image[imageBlk] * filter[filterBlk];
        }
    }

    image_out[tid] = acc;
}

int main(int argc, char ** argv) {

    dtype ** image_h, ** image_v_h, * image_out_h, * image_d, * image_out_d;
    uint     image_dy, image_dx;
    dtype ** filter_h, * filter_d;
    uint     filter_dy, filter_dx;

    readMatBinaryFile(IMAGE_FILE, &image_h, &image_dy, &image_dx);
    readMatBinaryFile(FILTER_FILE, &filter_h, &filter_dy, &filter_dx);
    createMat(&image_v_h, image_dy, image_dx);
    image_out_h = new dtype [image_dy * image_dx * sizeof(dtype)];

#if 0
    for (int i=0;i<image_dy;i++) {
        for (int j=0;j<image_dx;j++) {
            cout << image_h[i][j] << " ";
        }
        cout << endl;
    }
#endif

    cout << "Image size:  " << image_dx << "x" << image_dy << endl;
    cout << "Filter size: " << filter_dx << "x" << filter_dy << endl;

    applyFilter_host(
        image_h[0], image_dy, image_dx,
        filter_h[0], filter_dx, filter_dy,
        image_v_h[0]);

    size_t image_memReq = image_dy * image_dx * sizeof(dtype);
    size_t filter_memReq = filter_dy * filter_dx * sizeof(dtype);
    cudaMalloc(&image_d, image_memReq);
    cudaMalloc(&filter_d, filter_memReq);
    cudaMalloc(&image_out_d, image_memReq);
    cudaMemcpy(image_d, image_h[0], image_memReq, cudaMemcpyHostToDevice);
    cudaMemset(image_out_d, 0, image_memReq);
    cudaMemcpy(filter_d, filter_h[0], filter_memReq, cudaMemcpyHostToDevice);
    checkError("Memory Operations (h->d)");

    applyFilter_device<<<3000, 1024>>>(
        image_d, image_dy, image_dx,
        filter_d, filter_dx, filter_dy,
        image_out_d);
    cudaDeviceSynchronize();
    checkError("Apply Filter (device)");

    cudaMemcpy(image_out_h, image_out_d, image_memReq, cudaMemcpyDeviceToHost);  
    checkError("Memory Operations (d->h)");

    uint failedIdx, failedDiff;
    bool isPassed = verifyImageEqual(image_out_h, image_v_h[0], image_dy, image_dx, &failedIdx, &failedDiff);
    cout << "Verification: ";
    if (isPassed)
        cout << "PASSED" << endl;
    else
        cout << "FAILED at index [" << failedIdx << "], diff [" << failedDiff << "]" << endl;

    return 0;
}
