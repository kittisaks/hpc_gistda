#include <stdlib.h>

#include <iostream>

using namespace std;

#define ARRAY_SIZE 1000000
#define MIN_VALUE 10
#define MAX_VALUE 30000
#define BLOCKS 1000
#define THREADS_PER_BLOCK 1024

typedef int dtype;

void generateNumbers(dtype * array, size_t size) {

    srand(1);

    for (size_t i=0;i<size;i++) {
        array[i] = (rand() % MAX_VALUE) + MIN_VALUE;
    }
}

bool verify(dtype * array, size_t size, size_t * index) {
    for (size_t i=0;i<size-1;i++) {
        if (array[i] > array[i+1]) {
            *index = i;
            return false;
        }
    }
    return true;
}

__global__ void odd_even_sort(dtype * array, size_t arrSize, int * swap, int offset) {

    int tid = (blockIdx.x * blockDim.x) + threadIdx.x;
    __shared__ int lSwap;
    
    if (threadIdx.x == 0)
        lSwap = 0;
    __syncthreads();

    int idx = (tid * 2) + offset;
    if ((idx + 1) > arrSize)
        return;

    int t1 = array[idx];
    int t2 = array[idx + 1];
    __syncthreads();

    if (t1 > t2) {
        dtype temp = t1;
        t1 = t2;
        t2 = temp;
        lSwap = 1;
    }
    __syncthreads();

    array[idx] = t1;
    array[idx + 1] = t2;
    __syncthreads();

    if (threadIdx.x ==0)
        if (lSwap == 1)
            *swap = lSwap;
}

int main(int argc, char ** argv) {

    dtype * array_h, * array_d;
    int     _swap_h, * _swap_d;

    size_t arrSize = ARRAY_SIZE;
    array_h = new dtype [arrSize];
    generateNumbers(array_h, arrSize);

    cudaMalloc(&array_d, arrSize * sizeof(dtype));
    cudaMalloc(&_swap_d, sizeof(int));
    _swap_h = 0;
    cudaMemcpy(_swap_d, &_swap_h, sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(array_d, array_h, arrSize * sizeof(dtype), cudaMemcpyHostToDevice);

#if 0
    for (size_t idx=0;idx<arrSize;idx++)
        cout << array_h[idx] << " ";
    cout << endl;
#endif

    do {
        cudaMemset(_swap_d, 0, sizeof(int));
        odd_even_sort<<<BLOCKS, THREADS_PER_BLOCK>>>(array_d, arrSize, _swap_d, 0);
        cudaDeviceSynchronize();
        odd_even_sort<<<BLOCKS, THREADS_PER_BLOCK>>>(array_d, arrSize, _swap_d, 1);
        cudaDeviceSynchronize();
        cudaError_t ce = cudaGetLastError();
        if (ce != cudaSuccess)
            cout << "[ERROR]: Launch FAILED (" << cudaGetErrorString(ce) << ")" << endl;
        cudaMemcpy(&_swap_h, _swap_d, sizeof(int), cudaMemcpyDeviceToHost);
    } while (_swap_h == 1);
    cudaMemcpy(array_h, array_d, arrSize * sizeof(dtype), cudaMemcpyDeviceToHost);
#if 0
    for (size_t idx=0;idx<arrSize;idx++)
        cout << array_h[idx] << " ";
    cout << endl;
#endif

    cout << "Verification" << endl;
    size_t index;
    if (verify(array_h, arrSize, &index)) {
        cout << "PASSED";
    }
    else {
        cout << "FAILED at index [" << index << "]";
    }
    cout << endl;

    delete [] array_h;

    return 0;
}

