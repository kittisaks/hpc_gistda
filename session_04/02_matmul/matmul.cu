#include <cuda_runtime_api.h>

#include <iostream>

#include "matbin.h"

using namespace std;

#define MAT_A_FILE "matmul_dset/Y9000_X9000_00/matA_Y9000_X9000.bin" 
#define MAT_B_FILE "matmul_dset/Y9000_X9000_00/matB_Y9000_X9000.bin"
#define MAT_C_FILE "matmul_dset/Y9000_X9000_00/matC_Y9000_X9000.bin"

inline void checkErrorLoadMatrix(int ret, const char * filename) {
    if (!ret)
        return;
    cout << "[Error]: Cannot load " << filename << endl;
    exit(-1);
}

void transpose(
    dtype ** matt, uint * matty, uint * mattx,
    dtype ** mat,  uint   maty,  uint   matx) {

    for (int y=0;y<maty;y++) {
        for (int x=0;x<matx;x++) {
            matt[x][y] = mat[y][x];
        }
    }

    *mattx = maty;
    *matty = matx;
}

void checkError(const char * msg) {
    cout << msg << ": ";
    cudaError_t ce = cudaGetLastError();
    if (ce == cudaSuccess) {
        cout << "PASSED" << endl;
        return;
    }

    cout << "FAILED (" << cudaGetErrorString(ce)  << ")" << endl;
    exit(-1);
}

bool verifyMatEqual(dtype * mat, dtype * ref, uint dy, uint dx, uint * failedIdx, double * failedDiff) {

    uint totalLength = dy * dx;
    for (uint idx=0;idx<totalLength;idx++) {
        dtype diff = abs(mat[idx] - ref[idx]);
        double pdiff = ((double) diff) / ((double) ref[idx]);
        if (pdiff > 0.08) {
            cout << mat[idx] << " / " << ref[idx] << endl;
            *failedDiff = pdiff;
            *failedIdx = idx;
            return false;
        }
    }
    return true;
}

__global__ void matmul_device(
    dtype * matA,  uint ay,  uint ax,
    dtype * matBt, uint bty, uint btx,
    dtype * matC) {

    //TODO: Implement your kernel here

}

int main(int argc, char ** argv) {
    
    dtype ** matA_h, * matA_d;
    uint  matAx, matAy;
    dtype ** matB_h;
    uint  matBx, matBy;
    dtype ** matBt_h, * matBt_d;
    uint  matBtx, matBty;
    dtype ** matC_h, ** matCv, *matC_d;
    uint  matCx, matCy;

    checkErrorLoadMatrix(readMatBinaryFile(MAT_A_FILE, &matA_h, &matAy, &matAx), MAT_A_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_B_FILE, &matB_h, &matBy, &matBx), MAT_B_FILE);
    checkErrorLoadMatrix(readMatBinaryFile(MAT_C_FILE, &matCv, &matCy, &matCx), MAT_C_FILE);
    createMat(&matC_h, matAy, matBx);
    createMat(&matBt_h, matBx, matBy);
    transpose(matBt_h, &matBty, &matBtx, matB_h, matBy, matBx);

    cout << "Matrix A (YxX):  " << matAy << "x" << matAx << endl;
    cout << "Matrix B (YxX):  " << matBy << "x" << matBx << endl;
    cout << "Matrix B' (YxX): " << matBty << "x" << matBtx << endl;
    cout << "Matrix C (YxX):  " << matCy << "x" << matCx << endl;

    size_t a_memreq = matAy * matAx * sizeof(dtype);
    size_t bt_memreq = matBty * matBtx * sizeof(dtype);
    size_t c_memreq = matCy * matCx * sizeof(dtype);

    cudaMalloc(&matA_d, a_memreq);
    cudaMalloc(&matBt_d, bt_memreq);
    cudaMalloc(&matC_d, c_memreq);
    cudaMemcpy(matA_d, matA_h[0], a_memreq, cudaMemcpyHostToDevice);
    cudaMemcpy(matBt_d, matBt_h[0], bt_memreq, cudaMemcpyHostToDevice);
    cudaMemset(matC_d, 0, a_memreq);
    checkError("Memory Operation (h->d)");

    matmul_device<<<9000, 128>>>(matA_d, matAy, matAx, matBt_d, matBty, matBtx, matC_d);
    cudaDeviceSynchronize();
    checkError("Matmul Kernel");

    cudaMemcpy(matC_h[0], matC_d, c_memreq, cudaMemcpyDeviceToHost);
    checkError("Memory Operation (d->h)");

    uint failedIdx;
    double failedDiff;
    bool isPassed = verifyMatEqual(matC_h[0], matCv[0], matCy, matCx, &failedIdx, &failedDiff);
    cout << "Verification: ";
    if (isPassed)
        cout << "PASSED" << endl;
    else
        cout << "FAILED at index [" << failedIdx << "], diff [" << failedDiff << "]" << endl;

    return 0;
}

