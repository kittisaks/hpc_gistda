#include <mpi.h>
#include <unistd.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /**
     * All MPI processes split into two group. One contains only processes with Odd ranks whereas
     * another contains only the processe with even ranks
     */
    MPI_Comm myComm;
    int color = rank % 2;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &myComm);

    int newRank, newSize;

    MPI_Comm_rank(myComm, &newRank);
    MPI_Comm_size(myComm, &newSize);

    gethostname(hostname, 128);

    //Displaying global and local ranks of all processes
    cout << "[GLOBAL RANK- " << rank << "]: is executing on host [" << hostname << "] / "
       << "World Rank: " << rank << " - Local Rank: " << newRank << " - Local Size: " << newSize << endl;

    int * rankList = NULL;
    if (newRank == 0)
        rankList = new int [newSize];

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Gather(&rank, 1, MPI_INT, rankList, 1, MPI_INT, 0, myComm);

    //Display local result
    if (newRank == 0) {
        cout << "[GLOBAL RANK- " << rank << ", LOCAL RANK- " << newRank << "]: ";
        for (int i=0;i<newSize;i++)
            cout << rankList[i] << " ";
        cout << endl;
    }

    /**
     * Create inter-group communicator through the use of intra-communicator 
     */
    MPI_Comm leaderComm;
    MPI_Group groupAll, leaderGroup;
    MPI_Comm_group(MPI_COMM_WORLD, &groupAll);
    int leaderRanks[2] = {0, 1};
    MPI_Group_incl(groupAll, 2, leaderRanks, &leaderGroup);
    MPI_Comm_create(MPI_COMM_WORLD, leaderGroup, &leaderComm);

    int * globalRankList = NULL;
    if (rank == 0) {
        globalRankList = new int [size];
    }

    /**
     * Two process groups communicate using send/recv through global processes with rank 0
     */
    if ((rank == 0) || (rank == 1))
        MPI_Gather(rankList, newSize, MPI_INT, globalRankList, newSize, MPI_INT, 0, leaderComm);

    //Display global result
    if (rank == 0) {
        cout << "[GLOBAL RANK- " << rank << ", LOCAL RANK- " << newRank << "]: ";
        for (int i=0;i<size;i++)
            cout << globalRankList[i] << " ";
        cout << endl;
    }


    MPI_Finalize();

    return 0;
}

