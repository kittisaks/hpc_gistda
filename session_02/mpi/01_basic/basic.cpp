#include <mpi.h>
#include <unistd.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    gethostname(hostname, 128);

    cout << "[RANK- " << rank << "]: is executing on host [" << hostname << "]" << endl;

    sleep(5);

    MPI_Finalize();

    return 0;
}

