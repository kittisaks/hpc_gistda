#include <math.h>

#include <iomanip>
#include <iostream>

#include "nbbin.h"

#define REF_NBBIN "nb_d_10000_100.bin"
#define G_CONST 6.67e-6
#define MASS 1.0

using namespace std;

typedef vector<nbSnapshot_t*> SnapshotList;

void printDataset(nbDataset_t dset, uint32_t epIdx) {

    uint32_t cnt = dset->fileHeader.bodyCnt;
    SnapshotList * pList = dset->snapshots;

    nbSnapshot_t * pss = pList->at(0);
    nbSnapshot_t * ref_pss = pList->at(epIdx);

    system("clear");
    for (uint32_t idx=0;idx<10;idx++) {
        cout << std::fixed << std::setprecision(6) << pss->posX[idx] << "\t";
        cout << pss->posY[idx] << "\t";
        cout << pss->posZ[idx] << "\t";
        cout << pss->velX[idx] << "\t";
        cout << pss->velY[idx] << "\t";
        cout << pss->velZ[idx] << "\t";
        cout << pss->accX[idx] << "\t";
        cout << pss->accY[idx] << "\t";
        cout << pss->accZ[idx] << "\t";
        cout << endl;
    }

    cout << endl << endl << endl;
    for (uint32_t idx=0;idx<10;idx++) {
        cout << std::fixed << std::setprecision(6) << ref_pss->posX[idx] << "\t";
        cout << ref_pss->posY[idx] << "\t";
        cout << ref_pss->posZ[idx] << "\t";
        cout << ref_pss->velX[idx] << "\t";
        cout << ref_pss->velY[idx] << "\t";
        cout << ref_pss->velZ[idx] << "\t";
        cout << ref_pss->accX[idx] << "\t";
        cout << ref_pss->accY[idx] << "\t";
        cout << ref_pss->accZ[idx] << "\t";
        cout << endl;
    }
}

int verifyEpoch(nbSnapshot_t * pss, nbDataset_t dset, uint32_t epIdx, uint32_t * fIdx) {

    uint32_t cnt = dset->fileHeader.bodyCnt;
    SnapshotList * pList = dset->snapshots;
    nbSnapshot_t * ref_pss = pList->at(epIdx);

#define VERIFY(attr)                            \
    if (abs(pss->attr[idx]-ref_pss->attr[idx]) > 100.0) { \
        *fIdx = idx;                            \
        return -1;                              \
    }
    for (uint32_t idx=0;idx<cnt;idx++) {
        VERIFY(posX);
        VERIFY(posY);
        VERIFY(posZ);
    }
#undef VERIFY

    return 0;
}

void updateAttributes(nbSnapshot_t * pss, nbDataset_t ref) {
    uint32_t cnt = ref->fileHeader.bodyCnt;
    dtype timeDelta = ref->fileHeader.timeDelta;

    for (uint32_t iIdx=0;iIdx<cnt;iIdx++) {
        dtype forceX = 0.0;
        dtype forceY = 0.0;
        dtype forceZ = 0.0;
        for (uint32_t jIdx=0;jIdx<cnt;jIdx++) {

            if (iIdx == jIdx)
                continue;

            dtype distX = pss->posX[jIdx] - pss->posX[iIdx];
            dtype distY = pss->posY[jIdx] - pss->posY[iIdx];
            dtype distZ = pss->posZ[jIdx] - pss->posZ[iIdx];
            if (abs(distX) > 1e-4)
                forceX += ((G_CONST * distX) / abs(distX)) / (distX * distX);
            if (abs(distY) > 1e-4)
                forceY += ((G_CONST * distY) / abs(distY)) / (distY * distY);
            if (abs(distZ) > 1e-4)
                forceZ += ((G_CONST * distZ) / abs(distZ)) / (distZ * distZ);

        }

        dtype accX  = forceX / MASS;
        dtype accY  = forceY / MASS;
        dtype accZ  = forceZ / MASS;
        dtype velX  = pss->velX[iIdx] + (accX * timeDelta);
        dtype velY  = pss->velY[iIdx] + (accY * timeDelta);
        dtype velZ  = pss->velZ[iIdx] + (accZ * timeDelta);
        dtype posX  = pss->posX[iIdx] + ((pss->velX[iIdx] * timeDelta) + (0.5 * accX * timeDelta * timeDelta));
        dtype posY  = pss->posY[iIdx] + ((pss->velY[iIdx] * timeDelta) + (0.5 * accY * timeDelta * timeDelta));
        dtype posZ  = pss->posZ[iIdx] + ((pss->velZ[iIdx] * timeDelta) + (0.5 * accZ * timeDelta * timeDelta));

        //cout << "f " << iIdx << ": " << forceX << " - " << accX << " - " << velX << " - " << posX << endl;
        pss->posX[iIdx] = posX;
        pss->posY[iIdx] = posY;
        pss->posZ[iIdx] = posZ;
        pss->accX[iIdx] = accX;
        pss->accY[iIdx] = accY;
        pss->accZ[iIdx] = accZ;
        pss->velX[iIdx] = velX;
        pss->velY[iIdx] = velY;
        pss->velZ[iIdx] = velZ;

    }
 
}

int main(int argc, char ** argv) {

    nbDataset_t dset = nbLoadDatasetFromFile(REF_NBBIN);
    uint32_t bodyCnt = dset->fileHeader.bodyCnt;
    uint32_t epochCnt = dset->snapshots->size();

    cout << "===== Nbody =====" << endl;
    cout << "\tBody count:  " << bodyCnt << endl;
    cout << "\tEpoch count: " << epochCnt << endl;

    nbSnapshot_t * ss = dset->snapshots->at(0);
    uint32_t fIdx = -1;
    for (uint32_t ep=1;ep<epochCnt;ep++) {

        updateAttributes(ss, dset);
        cout << "Epoch[" << ep << "]: ";
        int rc = verifyEpoch(ss, dset, ep, &fIdx);
        if (!rc)
            cout << "PASSED" << endl;
        else {
            cout << "FAILED @ body " << fIdx << endl;
            exit(-1);
        }
        //printDataset(dset, ep+1);
    }

    return 0;
}
