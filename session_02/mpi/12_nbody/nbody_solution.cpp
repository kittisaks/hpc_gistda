#include <math.h>
#include <mpi.h>
#include <string.h>

#include <iomanip>
#include <iostream>

#include "nbbin.h"

#define REF_NBBIN "nb_d_10000_100.bin"
#define G_CONST 6.67e-6
#define MASS 1.0

using namespace std;

typedef vector<nbSnapshot_t*> SnapshotList;

typedef struct {
    int procCount;
    int procRank;
} MPI_Params;

typedef struct {
    uint32_t epochCnt;
    uint32_t bodyCnt;
    dtype    timeDelta;
} NB_Params;

void printDataset(nbDataset_t dset, uint32_t epIdx) {

    uint32_t cnt = dset->fileHeader.bodyCnt;
    SnapshotList * pList = dset->snapshots;

    nbSnapshot_t * pss = pList->at(0);
    nbSnapshot_t * ref_pss = pList->at(epIdx);

    system("clear");
    for (uint32_t idx=0;idx<cnt;idx++) {
        cout << std::fixed << std::setprecision(6) << "[" << idx << "]: " << pss->posX[idx] << "\t";
        cout << pss->posY[idx] << "\t";
        cout << pss->posZ[idx] << "\t";
        cout << pss->velX[idx] << "\t";
        cout << pss->velY[idx] << "\t";
        cout << pss->velZ[idx] << "\t";
        cout << pss->accX[idx] << "\t";
        cout << pss->accY[idx] << "\t";
        cout << pss->accZ[idx] << "\t";
        cout << endl;
    }

    cout << endl << endl << endl;
    for (uint32_t idx=0;idx<cnt;idx++) {
        cout << std::fixed << std::setprecision(6) << "[" << idx << "]: " << ref_pss->posX[idx] << "\t";
        cout << ref_pss->posY[idx] << "\t";
        cout << ref_pss->posZ[idx] << "\t";
        cout << ref_pss->velX[idx] << "\t";
        cout << ref_pss->velY[idx] << "\t";
        cout << ref_pss->velZ[idx] << "\t";
        cout << ref_pss->accX[idx] << "\t";
        cout << ref_pss->accY[idx] << "\t";
        cout << ref_pss->accZ[idx] << "\t";
        cout << endl;
    }
}

int verifyEpoch(nbSnapshot_t * pss, nbDataset_t dset, uint32_t epIdx, uint32_t * fIdx) {

    uint32_t cnt = dset->fileHeader.bodyCnt;
    SnapshotList * pList = dset->snapshots;
    nbSnapshot_t * ref_pss = pList->at(epIdx);

#define VERIFY(attr)                                       \
    if (abs(pss->attr[idx] - ref_pss->attr[idx]) > 100.0) {\
        *fIdx = idx;                                       \
        return -1;                                         \
    }
    for (uint32_t idx=0;idx<cnt;idx++) {
        VERIFY(posX);
        VERIFY(posY);
        VERIFY(posZ);
    }
#undef VERIFY

    return 0;
}

void updateAttributes(nbSnapshot_t * pss, NB_Params nbParams, MPI_Params mpiParams) {
    uint32_t cnt = nbParams.bodyCnt;        
    uint32_t proc_cnt = cnt / mpiParams.procCount;
    uint32_t proc_offset = mpiParams.procRank * proc_cnt;
    dtype timeDelta = nbParams.timeDelta;

    //TODO: May be optimized with Allgather
    MPI_Bcast(pss->posX, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->posY, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->posZ, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->velX, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->velY, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->velZ, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->accX, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->accY, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Bcast(pss->accZ, cnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);

    //TODO: Compute only the part the is required for the rank
    //cout << "[Rank-" << mpiParams.procRank << "]: " << proc_offset << " / " << proc_offset + proc_cnt << endl;
    for (uint32_t iIdx=proc_offset;iIdx<proc_offset + proc_cnt;iIdx++) {
        dtype forceX = 0.0;
        dtype forceY = 0.0;
        dtype forceZ = 0.0;
        for (uint32_t jIdx=0;jIdx<cnt;jIdx++) {

            if (iIdx == jIdx)
                continue;

            dtype distX = pss->posX[jIdx] - pss->posX[iIdx];
            dtype distY = pss->posY[jIdx] - pss->posY[iIdx];
            dtype distZ = pss->posZ[jIdx] - pss->posZ[iIdx];
            if (abs(distX) > 1e-4)
                forceX += ((G_CONST * distX) / abs(distX)) / (distX * distX);
            if (abs(distY) > 1e-4)
                forceY += ((G_CONST * distY) / abs(distY)) / (distY * distY);
            if (abs(distZ) > 1e-4)
                forceZ += ((G_CONST * distZ) / abs(distZ)) / (distZ * distZ);

        }

        dtype accX  = forceX / MASS;
        dtype accY  = forceY / MASS;
        dtype accZ  = forceZ / MASS;
        dtype velX  = pss->velX[iIdx] + (accX * timeDelta);
        dtype velY  = pss->velY[iIdx] + (accY * timeDelta);
        dtype velZ  = pss->velZ[iIdx] + (accZ * timeDelta);
        dtype posX  = pss->posX[iIdx] + ((pss->velX[iIdx] * timeDelta) + (0.5 * accX * timeDelta * timeDelta));
        dtype posY  = pss->posY[iIdx] + ((pss->velY[iIdx] * timeDelta) + (0.5 * accY * timeDelta * timeDelta));
        dtype posZ  = pss->posZ[iIdx] + ((pss->velZ[iIdx] * timeDelta) + (0.5 * accZ * timeDelta * timeDelta));

        pss->posX[iIdx] = posX;
        pss->posY[iIdx] = posY;
        pss->posZ[iIdx] = posZ;
        pss->accX[iIdx] = accX;
        pss->accY[iIdx] = accY;
        pss->accZ[iIdx] = accZ;
        pss->velX[iIdx] = velX;
        pss->velY[iIdx] = velY;
        pss->velZ[iIdx] = velZ;
    }

    size_t gatherCnt = cnt / mpiParams.procCount;
    size_t offset = mpiParams.procRank * gatherCnt;
    //TODO: May be optimized to AllGather
    MPI_Gather(pss->posX + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->posX, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->posY + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->posY, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->posZ + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->posZ, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->velX + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->velX, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->velY + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->velY, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->velZ + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->velZ, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->accX + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->accX, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->accY + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->accY, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
    MPI_Gather(pss->accZ + offset, gatherCnt * sizeof(dtype), MPI_CHAR, pss->accZ, gatherCnt * sizeof(dtype), MPI_CHAR, 0, MPI_COMM_WORLD);
}

int main(int argc, char ** argv) {

    int procRank, procCount;
    MPI_Params mpiParams;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &procRank);
    MPI_Comm_size(MPI_COMM_WORLD, &procCount);
    mpiParams.procCount = procCount;
    mpiParams.procRank = procRank;

    NB_Params      nbParams;
    nbDataset_t    dset      = NULL;
    nbSnapshot_t * ss        = NULL, * rss = NULL;
    uint32_t       bodyCnt   = 0, epochCnt = 0;
    dtype          timeDelta = 0.0;

    if (procRank == 0) {
        dset      = nbLoadDatasetFromFile(REF_NBBIN);
        bodyCnt   = dset->fileHeader.bodyCnt;
        epochCnt  = dset->snapshots->size();
        timeDelta = dset->fileHeader.timeDelta;

        nbParams.bodyCnt   = bodyCnt;
        nbParams.epochCnt  = epochCnt;
        nbParams.timeDelta = timeDelta;

        MPI_Bcast(&nbParams, sizeof(NB_Params), MPI_CHAR, 0, MPI_COMM_WORLD);

        cout << "===== Nbody =====" << endl;
        cout << "\tBody count:  " << bodyCnt << endl;
        cout << "\tEpoch count: " << epochCnt << endl;

        ss = dset->snapshots->at(0);
       
    }
    else {
        MPI_Bcast(&nbParams, sizeof(NB_Params), MPI_CHAR, 0, MPI_COMM_WORLD);

        bodyCnt   = nbParams.bodyCnt;
        epochCnt  = nbParams.epochCnt;
        timeDelta = nbParams.timeDelta;

        ss = new nbSnapshot_t();
        ss->posX = new dtype [bodyCnt];
        ss->posY = new dtype [bodyCnt];
        ss->posZ = new dtype [bodyCnt];
        ss->velX = new dtype [bodyCnt];
        ss->velY = new dtype [bodyCnt];
        ss->velZ = new dtype [bodyCnt];
        ss->accX = new dtype [bodyCnt];
        ss->accY = new dtype [bodyCnt];
        ss->accZ = new dtype [bodyCnt];
    }

    //For simplicity, we check if body count is multiple of number of processes
    if (nbParams.bodyCnt % mpiParams.procCount) {
        if (mpiParams.procRank == 0)
            cout << "[Error]: Body count should be multiple of processes" << endl;

        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    uint32_t fIdx = -1;
    for (uint32_t ep=1;ep<epochCnt;ep++) {
        updateAttributes(ss, nbParams, mpiParams);
        if (procRank == 0) {
            cout << "Epoch[" << ep << "]: ";
#if 1
            int rc = verifyEpoch(ss, dset, ep, &fIdx);
            if (!rc)
                cout << "PASSED" << endl;
            else {
                cout << "FAILED @ body " << fIdx << endl;
                //printDataset(dset, ep);
                MPI_Abort(MPI_COMM_WORLD, -1);
            }
#endif        
        }
    }

    MPI_Finalize();

    return 0;
}
