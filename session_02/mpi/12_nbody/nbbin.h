#ifndef _NBBIN_H
#define _NBBIN_H

#include <stdint.h>
#include <stdlib.h>

#include <vector>

typedef double dtype;

typedef struct {
    dtype timestamp;
} nbSnapshotHeader_t;

typedef struct {
    dtype * posX;
    dtype * posY;
    dtype * posZ;
    dtype * velX;
    dtype * velY;
    dtype * velZ;
    dtype * accX;
    dtype * accY;
    dtype * accZ;
} nbSnapshot_t;

typedef struct {
    uint32_t bodyCnt;
    dtype    timeDelta;
    uint32_t snapshotCnt;
} nbFileHeader_t;

typedef struct {
    nbFileHeader_t fileHeader;
    char * fileName;
    std::vector<nbSnapshot_t *> * snapshots;
    std::vector<nbSnapshotHeader_t> * snapshotHeaders;
} nbDataset_inst_t,* nbDataset_t;

nbDataset_t nbInitializeDataset(const char * filename, uint32_t bodyCnt, dtype timeDelta); 

int nbDataset_AddSnapshot(nbDataset_t dataset, dtype timestamp, nbSnapshot_t * snapshot);

int nbFinalize(nbDataset_t dataset);

void nbDestroyDataset(nbDataset_t dataset);

nbDataset_t nbLoadDatasetFromFile(const char * filename);


#endif /* _NBBIN_H */

