#include <mpi.h>
#include <string.h>

#include <iostream>

using namespace std;

#define RANK_NUMBER_COUNT 10
#define ROOT_RANK 1

int main(int argc, char ** argv) {

    float * rootNumbers;
    float myNumbers[RANK_NUMBER_COUNT];
    int rank, groupSize;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &groupSize);
    memset(myNumbers, 0, RANK_NUMBER_COUNT * sizeof(float));

    if (rank == ROOT_RANK) {
        rootNumbers = new float [groupSize * RANK_NUMBER_COUNT];
        cout << "[RANK " << rank << "]: Root-generated numbers = " << endl;
        for (int i=0;i<groupSize;i++)
            for (int j=0;j<RANK_NUMBER_COUNT;j++) {
                float number = static_cast<float>(i) + 
                    (static_cast<float>(j) / static_cast<float>(RANK_NUMBER_COUNT));
                rootNumbers[(i * RANK_NUMBER_COUNT) + j] = number;
                cout << number << "\t";
            }
        cout << endl << endl << endl << endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);

    float * gatheredNumbers;
    if (rank == ROOT_RANK) {
        gatheredNumbers = new float[groupSize * RANK_NUMBER_COUNT];
        memset(gatheredNumbers, 0, groupSize * RANK_NUMBER_COUNT * sizeof(float));
    }

    //////////////////////// SOLUTION ///////////////////////////
    MPI_Scatter(rootNumbers, RANK_NUMBER_COUNT, MPI_FLOAT,
        myNumbers, RANK_NUMBER_COUNT, MPI_FLOAT, ROOT_RANK, MPI_COMM_WORLD);
    ////////////////////////////////////////////////////////////

    cout << "[RANK " << rank << "]: Received numbers = " << endl;
    for (int i=0;i<RANK_NUMBER_COUNT;i++)
        cout << myNumbers[i] << "\t";
    cout << endl << endl;

    MPI_Finalize();

    return 0;
}

