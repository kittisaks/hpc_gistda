#include <mpi.h>
#include <string.h>

#include <iostream>

using namespace std;

#define RANK_NUMBER_COUNT 10
#define ROOT_RANK 1

int main(int argc, char ** argv) {

    float myNumbers[RANK_NUMBER_COUNT];
    int rank, groupSize;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &groupSize);
    for (int i=0;i<RANK_NUMBER_COUNT;i++) {
        float rank_f = static_cast<float>(rank);
        float count_f = static_cast<float>(RANK_NUMBER_COUNT);
        float i_f = static_cast<float>(i);
        myNumbers[i] = rank_f + (i_f / count_f);
    }

    cout << "[RANK " << rank << "]: Generated numbers = ";
    for (int i=0;i<RANK_NUMBER_COUNT;i++)
        cout << myNumbers[i] << "\t";
    cout << endl;

    float * gatheredNumbers;
    if (rank == ROOT_RANK) {
        gatheredNumbers = new float[groupSize * RANK_NUMBER_COUNT];
        memset(gatheredNumbers, 0, groupSize * RANK_NUMBER_COUNT * sizeof(float));
    }

    //////////////////////// SOLUTION ///////////////////////////
    
    //TODO: Add your implementation using MPI_Gather here
    
    
    ////////////////////////////////////////////////////////////

    if (rank == ROOT_RANK) {
        cout << endl << endl << endl << "[RANK " << rank << "]: Received numbers = " << endl;
        for (int i=0;i<groupSize * RANK_NUMBER_COUNT;i++)
            cout << gatheredNumbers[i] << "\t";
        cout << endl << endl;
        delete [] gatheredNumbers;
    }

    MPI_Finalize();

    return 0;
}

