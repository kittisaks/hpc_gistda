/*
 * mpiexec -np 10 --host localhost:20 --mca btl self,tcp ./ring
 */

#include <mpi.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    int rank, groupCount, nextRank, previousRank;
    char dataToNeighbor[128], dataFromNeighbor[128];
    size_t dataLength;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    memset(dataFromNeighbor, 0, 128);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &groupCount);
    nextRank = (rank + 1) % groupCount;
    previousRank = (rank - 1) < 0 ? groupCount - 1 : rank - 1;

    cout << "[RANK: " << rank << "]: Neighbor rank " << nextRank << endl;
    sprintf(dataToNeighbor, "Hello, this is the message from rank %d", rank);
    dataLength = strlen(dataToNeighbor);

    /////////////////////////////// SOLUTION /////////////////////////////////////////
#if 1
    MPI_Sendrecv(
        dataToNeighbor, dataLength, MPI_CHAR, nextRank, 0,
	dataFromNeighbor, 128, MPI_CHAR, previousRank, 0, MPI_COMM_WORLD, &status);
#else
    MPI_Send(dataToNeighbor, dataLength, MPI_CHAR, nextRank, 0, MPI_COMM_WORLD);
    MPI_Recv(dataFromNeighbor, 128, MPI_CHAR, previousRank, 0, MPI_COMM_WORLD, &status); 
#endif
    //////////////////////////////////////////////////////////////////////////////////

    cout << "[RANK: " << rank << "]: message received: " << dataFromNeighbor << endl;

    MPI_Finalize();

    return 0;
}
