#include <mpi.h>
#include <unistd.h>

#include <iostream>

#define NUM_COLORS 2

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Comm newComm;
    int color = rank % NUM_COLORS;
    int newRank, newSize;

    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &newComm);
    MPI_Comm_size(newComm, &newSize);
    MPI_Comm_rank(newComm, &newRank);

    gethostname(hostname, 128);

    cout << "[RANK- " << rank << "]: is executing on host [" << hostname << "] / "
       << "World Rank: " << rank << " - New Rank: " << newRank << " - New Size: " << newSize << endl;

    MPI_Finalize();

    return 0;
}

