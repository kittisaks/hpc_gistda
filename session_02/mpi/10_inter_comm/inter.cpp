#include <mpi.h>
#include <string.h>
#include <unistd.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    /**
     * All MPI processes split into two group. One contains only processes with Odd ranks whereas
     * another contains only the processe with even ranks
     */
    MPI_Comm myComm;
    int color = rank % 2;
    MPI_Comm_split(MPI_COMM_WORLD, color, rank, &myComm);

    int newRank, newSize;

    MPI_Comm_rank(myComm, &newRank);
    MPI_Comm_size(myComm, &newSize);

    gethostname(hostname, 128);

    //Displaying global and local ranks of all processes
    cout << "[RANK- " << rank << "]: is executing on host [" << hostname << "] / "
       << "World Rank: " << rank << " - New Rank: " << newRank << " - New Size: " << newSize << endl;

    int * rankList = NULL;
    if (newRank == 0)
        rankList = new int [newSize];

    MPI_Gather(&rank, 1, MPI_INT, rankList, 1, MPI_INT, 0, myComm);
    MPI_Barrier(MPI_COMM_WORLD);

    //Displaying local result
    if (newRank == 0) {
        cout << "[GRANK- " << rank << ", LRANK- " << newRank << "]: ";
        for (int i=0;i<newSize;i++)
            cout << rankList[i] << " ";
        cout << endl;
    }
    

    /**
     * The inter-communicator is created here for exchanging data between the two groups
     */
    MPI_Comm interComm;
    MPI_Comm_rank(myComm, &newRank);
    MPI_Comm_size(myComm, &newSize);
    if ((rank % 2) == 1)
        MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 0, 0, &interComm);
    else if ((rank % 2) == 0)            
        MPI_Intercomm_create(myComm, 0, MPI_COMM_WORLD, 1, 0, &interComm);


    /**
     * Two process groups communicate using send/recv through local processes with rank 0
     */
    int globalRankList[128];
    if ((rank % 2) == 0) {
        if (newRank == 0) {
            memcpy(globalRankList, rankList, newSize * sizeof(int));
            MPI_Recv(globalRankList + newSize, size, MPI_INT, 0, 0, interComm, NULL);
        }
    }
    else if ((rank % 2) == 1) {
        if (newRank == 0)
            MPI_Send(rankList, newSize, MPI_INT, 0, 0, interComm);
    }

    //Displaying global result
    if (rank == 0) {
        cout << "[GLOBAL RANK- " << rank << ", LOCAL RANK- " << newRank << "]: ";
        for (int i=0;i<size;i++)
            cout << globalRankList[i] << " ";
        cout << endl;
    }

    MPI_Finalize();

    return 0;
}

