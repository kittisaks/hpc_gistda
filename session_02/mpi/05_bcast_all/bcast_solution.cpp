#include <mpi.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    int rank, groupCount;
    char dataToNeighbor[128], ** dataFromNeighbor;
    size_t dataLength;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &groupCount);
    dataFromNeighbor = new char * [groupCount];
    for (int i=0;i<groupCount;i++) {
	dataFromNeighbor[i] = new char[128];
        memset(dataFromNeighbor[i], 0, 128);
    }
    sprintf(dataToNeighbor, "Hello, this is the message from rank %d", rank);
    dataLength = strlen(dataToNeighbor);

    for (int i=0;i<groupCount;i++) {
        char * buffer = (i == rank) ? dataToNeighbor : dataFromNeighbor[i];
    /////////////////////////////// SOLUTION /////////////////////////////////////////
        MPI_Bcast(buffer, 128, MPI_CHAR, i, MPI_COMM_WORLD);
    //////////////////////////////////////////////////////////////////////////////////
	if (i == rank)
            sprintf(dataFromNeighbor[i], "THIS IS MY SLOT; IGNORE THIS SLOT");
    }

    cout << "[RANK: " << rank << "]: message received: " << endl;
    for (int i=0;i<groupCount;i++) {
        cout << "\t" << dataFromNeighbor[i] << endl;
    }
    cout << endl << endl;

    for (int i=0;i<groupCount;i++)
        delete [] dataFromNeighbor[i];
    delete [] dataFromNeighbor;
    MPI_Finalize();

    return 0;
}
