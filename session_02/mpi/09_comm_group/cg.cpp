#include <mpi.h>
#include <unistd.h>

#include <iostream>

#define NUM_COLORS 2

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Group groupAll, newGroup;
    int oddList[128], evenList[128];
    int oddListSize = 0, evenListSize = 0;
    MPI_Comm newComm;
    int newRank, newSize;

    MPI_Comm_group(MPI_COMM_WORLD, &groupAll);
    for (int i=0;i<size;i++) {
        if (i % 2) {
            oddList[oddListSize] = i;
            oddListSize++;
        }
        else {
            evenList[evenListSize] = i;
            evenListSize++;
        }

    }

    if (rank % 2) 
        MPI_Group_incl(groupAll, oddListSize, oddList, &newGroup);
    else
        MPI_Group_incl(groupAll, evenListSize, evenList, &newGroup);
    MPI_Comm_create_group(MPI_COMM_WORLD, newGroup, 0, &newComm);

    MPI_Comm_rank(newComm, &newRank);
    MPI_Comm_size(newComm, &newSize);

    gethostname(hostname, 128);

    cout << "[RANK- " << rank << "]: is executing on host [" << hostname << "] / "
       << "World Rank: " << rank << " - New Rank: " << newRank << " - New Size: " << newSize << endl;

    MPI_Finalize();

    return 0;
}

