#include <mpi.h>
#include <unistd.h>

#include <iostream>

#define NUM_COLORS 2

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank, size;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Group groupAll, oddGroup, evenGroup;
    int oddList[128], evenList[128];
    int oddListSize = 0, evenListSize = 0;
    MPI_Comm oddComm, evenComm;
    int newRank, newSize;

    MPI_Comm_group(MPI_COMM_WORLD, &groupAll);
    for (int i=0;i<size;i++) {
        if (i % 2) {
            oddList[oddListSize] = i;
            oddListSize++;
        }
        else {
            evenList[evenListSize] = i;
            evenListSize++;
        }

    }

    MPI_Group_incl(groupAll, oddListSize, oddList, &oddGroup);
    MPI_Group_incl(groupAll, evenListSize, evenList, &evenGroup);
    MPI_Comm_create_group(MPI_COMM_WORLD, oddGroup, 0, &oddComm);
    MPI_Comm_create_group(MPI_COMM_WORLD, evenGroup, 0, &evenComm);

    MPI_Comm_rank(oddComm, &newRank);
    MPI_Comm_size(oddComm, &newSize);

    gethostname(hostname, 128);

    cout << "[RANK- " << rank << "]: is executing on host [" << hostname << "] / "
       << "World Rank: " << rank << " - New Rank: " << newRank << " - New Size: " << newSize << endl;

    MPI_Finalize();

    return 0;
}

