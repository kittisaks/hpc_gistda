#include <mpi.h>
#include <unistd.h>

#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    char hostname[128];
    int  rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    cout << "[RANK- " << rank << "]: receives " << argc << " arguments: ";
    for (int i=0;i<argc;i++)
        cout << argv[i] << " / "; 
    cout  << endl;

    sleep(5);

    MPI_Finalize();

    return 0;
}

